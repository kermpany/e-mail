"KLI": {
    name: "Kuala Lumpur International Airport".into(),
    code: "KLI",
    lat: 2.739468,
    lon: 101.705757,
    flights: {
        "LHR": [
            { stop: "LHR", duration: 49176 }
        ],
        "SFO": [
            { stop: "LHR", duration: 49176 },
            { stop: "SFO", duration: 16488 }
        ]
        "ORD": [
            { stop: "LHR", duration: 49176 },
            { stop: "ORD", duration: 30888 }
        ]
    }
}
"LHR": {
    name: "London Heathrow Airport".into(),
    code: "LHR",
    lat: 51.470219,
    lon: -0.454558,
    flights: {
        "ORD": [
            { stop: "ORD", duration: 30888 }
        ],
        "SFO": [
            { stop: "SFO", duration: 39600 }
        ]
        "KLI": [
            { stop: "KLI", duration: 47988 }
        ]
    }
},
"SFO": {
    code: "SFO",
    name: "San Francisco International Airport".into(),
    lat: 37.618624,
    lon: -122.379649,
    flights: {
        "LHR": [
            { stop: "LHR", duration: 36900 }
        ],
        "ORD": [
            { stop: "ORD", duration: 15588 }
        ]
        "KLI": [
            { stop: "LHR", duration: 36900 },
            { stop: "KLI", duration: 47988 }
        ]
    }
},
"ORD": {
    name: "Chicago O'hare Airport".into()
    code: "ORD"
    lat: 41.975381,
    lon: -87.901671,
    flights: {
        "LHR": [
            { stop: "LHR", duration: 27900 }
        ],
        "SFO": [
            { stop: "SFO", duration: 16488 }
        ]
        "KLI": [
            { stop: "LHR", duration: 27900 },
            { stop: "KLI", duration: 47988 }
        ]
    }
}
