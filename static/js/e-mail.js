if(document.getElementById("e-mail-section")) {
    var vm = new Vue({
      el: '#e-mail-section',

      data: {
        loading: true,
        trackingNumber: window.location.pathname.split("/")[2],
        info: {},
        zoomPoint: [],
        origin: null,
        destination: null,
        route: {},
        point: {},
        lineDistance: null,
        status: "",
        unopened_letters: [],
        letters: [],
        letter_ids: [],
        senderEmail: "",
        recipientEmail: ""
      },

      filters: {
        momentDate: function (date) {
            return moment(date).format('MMM Do');
        },
        momentTime: function(date) {
        let utcDate = moment.tz(date,'UTC');
        let localDate = moment.utc(utcDate).local();

        return localDate.format('h:mm:ssa');
        }
      },

      methods: {
        clampdom: function(fro, to) {
            return Math.floor(Math.abs(to - fro) * Math.random()) + ((to+fro)/2);
        },
        calculateStampStyles: function() {
            var self = this;

            return {
                arr: "top:"+self.clampdom(16,22)+"px; left:"+self.clampdom(32,48)+"px; transform: rotate("+self.clampdom(-3,3)+"deg);",
                box: "top:"+self.clampdom(134,200)+"px; left:"+self.clampdom(16,32)+"px; transform: rotate("+self.clampdom(-10,4)+"deg);",
                sent: "top:"+self.clampdom(-2, 4)+"px; left:"+self.clampdom(32, 48)+"px; transform: rotate("+self.clampdom(-3,3)+"deg);"
            };

        },
        loadLetter: function(e) {
          var self = this;
          var tracking = e.target.dataset.tracking;
            axios.get('/api/e-mail/'+tracking)
            .then(function(resp) {
                self.letterFromId(resp.data, tracking)
            })
            .catch(function(err) {
                console.log(err);
                for(var i = 0; i < self.letters.length; i++) {
                    var letter = self.letters[i];
                    letter.unopenable = false;
                    letter.tried = true;
                    if(letter.id == tracking) {
                        letter.unopenable = true;
                        break;
                    }
                }
            });
        },
        letterFromId: function(letterData, tracking) {
            var self = this;
            for(var i = 0; i < self.letters.length; i++) {
                var letter = self.letters[i];
                let utcDate = moment.tz(letterData.tracking_info.time_mailed,'UTC');
                let localSentDate = moment.utc(utcDate).local();
                utcDate = moment.tz(letterData.tracking_info.eta,'UTC');
                // TODO: make actual arrival time AND static to arrival destinatino TZ
                let localArrivalDate = moment.utc(utcDate).local();
                if(letter.id == tracking) {
                    letter.sender = letterData.tracking_info.sender_email;
                    letter.senderLoc = letterData.tracking_info.origin;
                    letter.recipient = letterData.tracking_info.recipient_email;
                    letter.recipientLoc = letterData.tracking_info.destination;
                    letter.sent = localSentDate.format("MMM D, YYYY");
                    letter.arrived = localArrivalDate.format("MMM D, YYYY");
                    letter.body = self.deConfuseLetter(letterData.letter);
                    letter.unopened = false;
                    letter.inview = false;
                    break;
                }
            }
        },
        letterFromData: function(letterData, tracking) {
            var self = this;
            var index = self.letterIds.length - 1 - self.letterIds.indexOf(tracking);
            console.log(letterData.tracking_info.time_mailed);
            let utcDate = moment.tz(letterData.tracking_info.time_mailed,'UTC');
            let localSentDate = moment.utc(utcDate).local();
            utcDate = moment.tz(letterData.tracking_info.eta,'UTC');
            // TODO: make actual arrival time AND static to arrival destinatino TZ
            let localArrivalDate = moment.utc(utcDate).local();
            return {
                id: tracking,
                sortIndex: index,
                sender: letterData.tracking_info.sender_email,
                senderLoc: letterData.tracking_info.origin,
                recipient: letterData.tracking_info.recipient_email,
                recipientLoc: letterData.tracking_info.destination,
                sent: localSentDate.format("MMM D, YYYY"),
                arrived: localArrivalDate.format("MMM D, YYYY"),
                body: self.deConfuseLetter(letterData.letter),
                styles: self.calculateStampStyles(),
                unopened: false,
                inview: false,
                unopenable: false,
            };
        },
        deConfuseLetter: function (letter) {
            return letter.replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);});
        },
        addEmptyLetters: function() {
            var self = this;
            var mainIndex = self.letterIds.indexOf(self.trackingNumber);
            for(var i = mainIndex - 1; i >= 0; i--) {
                var lid = self.letterIds[i];
                var index = self.letterIds.length - 1 - i;
                var letter = {
                    id: lid,
                    sortIndex: index,
                    sender: "",
                    senderLoc: "",
                    recipient: "",
                    recipientLov: "",
                    sent: "",
                    arrived: "",
                    body: "",
                    styles: self.calculateStampStyles(),
                    unopened: true,
                    inview: false,
                    unopenable: true,
                    tried: false,
                };
                self.letters.push(letter);
            }
            for(var i = mainIndex + 1; i < self.letterIds.length; i++) {
                var lid = self.letterIds[i];
                var index = self.letterIds.length - 1 - i;
                var letter = {
                    id: lid,
                    sortIndex: index,
                    sender: "",
                    senderLoc: "",
                    recipient: "",
                    recipientLov: "",
                    sent: "",
                    arrived: "",
                    body: "",
                    styles: self.calculateStampStyles(),
                    unopened: true,
                    inview: false,
                    unopenable: true,
                    tried: false,
                };
                self.letters.unshift(letter);
            }
        }
      },
      created: function() {
        var self = this;
        axios.get('/api/e-mail/' + self.trackingNumber)
        .then(function(resp) {
            self.loading = false;
            self.letterIds = resp.data.letters;
            self.letters = [self.letterFromData(resp.data, self.trackingNumber)];
            self.addEmptyLetters();

            var info = resp.data.tracking_info;
            info.time_posted = moment.tz(info.time_posted,'UTC'); //moment.parseZone(info.time_posted, "X");
            info.time_mailed = moment.tz(info.time_mailed,'UTC'); //moment.parseZone(info.time_mailed, "X");
            info.eta = moment.parseZone(info.eta, "X");

            self.info = info;

            self.replyString = "?rec=" + encodeURI(self.info.sender_email) +
                "&snd=" + encodeURI(self.info.recipient_email) +
                "&sloc=" + encodeURI(self.info.destination) +
                "&slon=" + encodeURI(self.info.destination_lon) +
                "&slat=" + encodeURI(self.info.destination_lat) +
                "&rloc=" + encodeURI(self.info.origin) +
                "&rlon=" + encodeURI(self.info.origin_lon) +
                "&rlat=" + encodeURI(self.info.origin_lat) +
                "&pt=" + encodeURI(self.trackingNumber);

            self.origin = [self.info.origin_lon, self.info.origin_lat];
            self.destination = [self.info.destination_lon, self.info.destination_lat];
            self.route = {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [
                            self.origin,
                            self.destination
                        ]
                    }
                }]
            };
            self.point = {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": self.origin
                    }
                }]
            };

            self.lineDistance = turf.lineDistance(self.route.features[0], 'kilometers');
            var arc = [];

            // Draw an arc between the `origin` & `destination` of the two points
            for (var i = 1; i < 1000; i++) {
              var segment = turf.along(self.route.features[0], i / 1000 * self.lineDistance, 'kilometers');
              arc.push(segment.geometry.coordinates);
            }

            self.route.features[0].geometry.coordinates = arc;

            self.lineDistance = turf.lineDistance(self.route.features[0], 'kilometers');
            var midPoint = turf.along(self.route.features[0], .5 * self.lineDistance, 'kilometers').geometry.coordinates;
            var avgPoint = [(self.destination[0] + self.origin[0]) / 2, (self.destination[1] + self.origin[1]) / 2];

            var bonusMiles = 0;
            if(self.origin[0] * self.destination[0] < 0) {
              var distDir1 = Math.abs(self.origin[0]) + Math.abs(self.destination[0]);
              var distDir2 = 360 - distDir1;
              bonusMiles = Math.min(distDir1, distDir2) == distDir1 ? 0 : 180;
            }

            self.zoomPoint[0] = avgPoint[0] + bonusMiles;
            self.zoomPoint[1] = (midPoint[1] + avgPoint[1])/2;

            var boundingBox = [[0,0],[0,0]]; //sw , ne
            if(self.origin[0] < self.destination[0]) {
              boundingBox[0][0] = self.origin[0] - 4 + bonusMiles;
              boundingBox[1][0] = self.destination[0] + 4 + bonusMiles;
            }
            else {
              boundingBox[0][0] = self.destination[0] - 4 + bonusMiles;
              boundingBox[1][0] = self.origin[0] + 4 + bonusMiles;
            }

            boundingBox[0][1] = Math.min(midPoint[1], Math.min(self.origin[1], self.destination[1])) - 4;
            boundingBox[1][1] = Math.max(midPoint[1], Math.max(self.origin[1], self.destination[1])) + 4;

            boundingBox[0][1] = boundingBox[0][1] < -90 ? -90 : boundingBox[0][1];
            boundingBox[1][1] = boundingBox[1][1] > 90 ? 90 : boundingBox[1][1];

            var zoom = 4.3 - .00032 * self.lineDistance;
            zoom = zoom < 0 ? 0 : zoom;

            mapboxgl.accessToken = 'pk.eyJ1IjoibWNnaW50eSIsImEiOiJjaXhwMjlmdnYwYTVtMzNwdTdzb2xzOWR0In0.gjXgJ256P1CIFUGBtwo4Fg';
            self.map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mcginty/cixqaghqo005z2rqsl6kl0adv',
                center: self.zoomPoint,
                zoom: zoom,
            });
            self.map.fitBounds(boundingBox);

            self.map.on('load', function () {
                // Add a source and layer displaying a point which will be animated in a circle.
                self.map.addSource('route', {
                    "type": "geojson",
                    "data": self.route
                });

                self.map.addSource('point', {
                    "type": "geojson",
                    "data": self.point
                });

                self.map.addLayer({
                    "id": "route",
                    "source": "route",
                    "type": "line",
                    "paint": {
                        "line-width": 3,
                        "line-color": "#ff2e88"
                    }
                });

                self.map.addLayer({
                    "id": "point",
                    "source": "point",
                    "type": "symbol",
                    "layout": {
                        "icon-image": "post-15",
                    }
                });
            });
        })
        .catch(function(error) {
          if (error.response) {
            console.log("Network error", error);
            window.location.href = "/?msg=embt";
          } else {
            console.log("Error in our logic, caught by axios", error);
          }
        });
      }
    });
}
