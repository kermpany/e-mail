if(document.getElementById("index-page")) {

  var vm = new Vue({

    el: '#index-page',

    data: {
      clean: {'body': true, 'sender_email': true, 'sender_location': true, 'recipient_email': true, 'recipient_location': true},
      loading: false,
      done: false,
      emailButton: "send it!",
      canGeolocate: !!navigator.geolocation,
      messageCode: window.location.search.split("=")[1],
      messageClosed: false,
      form: {
        sender: {
          email: "",
          location: { lat: null, lon: null, name: ""}
        },
        recipient: {
          email: "",
          location: { lat: null, lon: null, name: ""}
        },
        body: ""
      },
      suggestions: {sender: [], recipient: []},
      hasSuggestions: {sender: false, recipient: false},
      focusedSug: 0,
      sugsToFocus: [0,0,0,0],
      params: {},
      errors: {},
      sendDisabled: true,
      tickerBlob: {},
      emptyTicker: false,
      locationFromId: {
        1733046: "Kuala Lumpur",
        4887398: "Chicago",
        5391959: "San Francisco",
        2643743: "London"
      },
      showWeather: true,
      alertTicker: false,
      showLope: false,
      bad: [202, 211, 212, 221, 232, 502, 503, 504, 522, 531, 612, 622, 711, 731, 762, 78],
      good: [800, 801, 802],
      baddies: [
        "Welp, we don’t claim to tame the weather...",
        "Should be moving shortly!",
        "Sorry for the delay >_<",
        "One day we’ll make weather resistant planes",
        "Kermp is doing the best to calm the rain gods.",
        "That e-mail wasn’t time sensitive i hope.",
        "Rain Rain Go Awaaaaay!",
        "Rain drops keep fallin' on my head...",
        "Delays, delays, delays!",
        "...just keep fallin!",
        "Send more! We are stockpiling!"
      ],
      riskies: [
        "Not sunny, but hey we're still flying",
        "Could be worse!",
        "Probably nothing to worry about",
        "just here to say hi",
        "All according to plan :D",
        "Don't worry your e-mail is on the go!",
        "Doop Doop Doop",
      ],
      goodies: [
        "That's plane flyin' weather!",
        "Everything is movin' and groovin'",
        "How about this weather?",
        "Bad day? At least skies aren't grey!",
        "I'm walkin' on SUNSHINE",
        "What refreshing weather!",
        "And don't it feel good!"
      ]
    },
    created: function () {
      var self = this;
      self.parseParams();

      if(self.params['msg']) {
        self.setMessage();
      }
      if(self.params['snd']) {
        self.fillForm();
      }
      else if(sessionStorage.length > 0) {
        self.loadFromSessionStorage();
      }
      self.getTicker();

      self.shuffleWeatherOpinions();
    },
    methods: {
      loadFromSessionStorage: function() {
        var self = this;
        if(sessionStorage.sender_email) {
          self.form.sender.email = sessionStorage.sender_email;
          self.validateEmail({target: {dataset: {which: "sender_email"}, value: sessionStorage.sender_email}});
          delete self.clean["sender_email"];
        }
        if(sessionStorage.recipient_email) {
          self.form.recipient.email = sessionStorage.recipient_email;
          self.validateEmail({target: {dataset: {which: "recipient_email"}, value: sessionStorage.recipient_email}});
          delete self.clean["recipient_email"];
        }
        if(sessionStorage.sender_location) {
          self.form.sender.location = JSON.parse(sessionStorage.sender_location);
          self.validateLocation({target: {dataset: {which: "sender"}}});
          delete self.clean["sender_location"];
        }
        if(sessionStorage.recipient_location) {
          self.form.recipient.location = JSON.parse(sessionStorage.recipient_location);
          self.validateLocation({target: {dataset: {which: "recipient"}}});
          delete self.clean["recipient_location"];
        }
        if(sessionStorage.body) {
          self.form.body = sessionStorage.body;
          self.validateBody({target: {value: sessionStorage.body}});
          delete self.clean["body"];
        }
      },
      allValid: function() {
        var self = this;
        self.sendDisabled = Object.keys(self.clean).length > 0 || Object.keys(self.errors).length > 0;
      },
      validateBody: function(e) {
        var self = this;

        var value = e.target.value;
        var field = "body";

        if(value.length > 0) {
          delete self.errors[field];
          delete self.clean[field];
          sessionStorage.setItem(field, value);
        }
        else {
          self.errors[field] = true;
          sessionStorage.removeItem(field);
        }
        self.allValid();

      },
      validateEmail: function(e) {
        var self = this;
        var emailreg = /^[^@\s]+@[^@\s]+\.[^@\s]+$/;
        var field = e.target.dataset.which;
        var value = e.target.value;
        if(emailreg.test(value)) {
          delete self.errors[field];
          delete self.clean[field];
        }
        else {
          self.errors[field] = true;
        }
        self.allValid();

        if(value.length > 0) {
          sessionStorage.setItem(field, value);
        }
        else {
          sessionStorage.removeItem(field);
        }
      },
      validateLocation: function(e) {
        var self = this;
        var which = e.target.dataset.which;
        var field = which + "_location";
        var loc = self.form[which].location;

        if(loc.name !== "" && loc.lat != null && loc.lon != null) {
          delete self.errors[field];
          delete self.clean[field];
          sessionStorage.setItem(field, JSON.stringify(loc));
        }
        else {
          self.errors[field] = true;
          sessionStorage.removeItem(field);
        }
        self.allValid();
      },
      fillForm: function() {
        var self = this;

        self.form.sender = {
          'email': self.params.snd,
          'location': {'lat': parseFloat(self.params.slat), 'lon': parseFloat(self.params.slon), 'name': self.params.sloc}
        };

        self.form.recipient = {
          'email': self.params.rec,
          'location': {'lat': parseFloat(self.params.rlat), 'lon': parseFloat(self.params.rlon), 'name': self.params.rloc}
        };

        self.form.prev_tracking = self.params.pt;
        self.validateEmail({target: {dataset: {which: "sender_email"}, value: self.params.snd}});
        self.validateEmail({target: {dataset: {which: "recipient_email"}, value: self.params.rec}});
        self.validateLocation({target: {dataset: {which: "sender"}}});
        self.validateLocation({target: {dataset: {which: "recipient"}}});

        delete self.clean["sender_email"];
        delete self.clean["sender_location"];
        delete self.clean["recipient_email"];
        delete self.clean["recipient_location"];
        
        self.allValid();
      },
      parseParams: function() {
        var self = this;
        var query = window.location.search;
        var query = query.slice(1, query.length);
        query.split("&").forEach(function(param) {
          param = param.split("=");
          self.params[param[0]] = decodeURI(param[1]);
        });
      },
      setMessage: function() {
        var message = window.location.search.split("=")[1];

        if(window.location.search.indexOf("?msg=")) { return; }

        switch (message) {
          case 'trbt':
            return "Hmm.... we don't have that tracking number";
          case 'trnk':
            return "Welp, that tracking number was invisible or something";
          case 'embt':
            return "That email has disappeared! ...or never existed :O";
          case 'emnk':
            return "were you looking for just any old e-mail? try with a tracking number";
          default:
            return null;
        }
      },
      showMessage: function() {
        var self = this;

        return window.location.search.indexOf("?msg=") >= 0 && self.messageCode != null && self.messageCode.length > 0;
      },
      handleKeyUp: function(e) {
        var self = this;
        var key = e.keyCode;
        var which = e.target.dataset.which;
        self.focusedSug = self.focusedSug < 0 ? 0 : self.focusedSug;

        switch (key) {
          case 9: // tab
          case 16: // shift
          case 37: // left
          case 39: // right
          case 91: // meta left
          case 93: // meta right
          case 18: // alt/option
          case 17: // ctrl
            break;
          case 38: //up
            self.focusedSug = self.focusedSug <= 0 ? 0 : self.focusedSug - 1;
            break;
          case 40: //down
            self.focusedSug = self.focusedSug >= self.suggestions[which].length - 1 ? self.suggestions[which].length - 1 : self.focusedSug + 1;
            break;
          case 13: //enter
            self.completeAutocomplete({target: {dataset: {lat:self.suggestions[which][self.focusedSug].center[1], lon:self.suggestions[which][self.focusedSug].center[0], name:self.suggestions[which][self.focusedSug].place_name, which: which}}});
            break;
          case 27: //esc
            document.activeElement.blur();
          default:
            self.autocompleteLocation(e);
        };

        self.sugsToFocus = [0,0,0,0];
        self.sugsToFocus[self.focusedSug] = 1;
      },
      autocompleteLocation: _.throttle(function(e) {
        var self = this;
        var data = e.target.dataset;
        var which = data.which;
        var name;

        if (which == "sender") {
          name = self.form.sender.location.name;
        } else {
          name = self.form.recipient.location.name;
        }
        if(name.length >= 1) {
            axios.get('https://api.mapbox.com/geocoding/v5/mapbox.places/'+ encodeURIComponent(name)+'.json?access_token=pk.eyJ1IjoiZ2VsaW5rIiwiYSI6ImNpcG55OWk2MDAwMHRoem5iY3draDZvdXQifQ.hpO3gotMwQRjwCiPsWQmAQ&types=place&autocomplete=true&limit=4')
            .then(function(resp) {
              var focusEl = document.activeElement.id;
              if(focusEl == "sender_location" || focusEl == "recipient_location") {
                self.hasSuggestions[which] = true;

                var places = resp.data["features"];

                for (var i=0; i < 4; i++) {
                  if(places[i]) {
                    Vue.set(self.suggestions[which], i, places[i]);
                  } else {
                    self.suggestions[which].pop();
                  }
                }
              }
            })
            .catch(function(error) {
              console.error("ajax error!");
              console.error(error);
            });
        } else if(name == 0) {
          self.hasSuggestions[which] = false;
        }
        self.form[which].location.lat = null;
        self.form[which].location.lon = null;
        self.validateLocation(e);
      }, 200, { leading: true }),
      cancelAutocomplete: function(e) {
        var self = this;
        var which = e.target.dataset.which;
        self.hasSuggestions[which] = false;
      },
      completeAutocomplete: function(e) {
        var self = this;
        var data = e.target.dataset;
        self.focusedSug = 0;
        //shove the stuff in the stuff
        self.hasSuggestions[data.which] = false;
        self.form[data.which].location.name = data.name;
        self.form[data.which].location.lat = parseFloat(data.lat);
        self.form[data.which].location.lon = parseFloat(data.lon);
        self.validateLocation(e);
      },
      completeDidyamean: function(e) {
        var self = this;
        var which = e.target.dataset.which;
        self.form[which].location.name = self.suggestions[which][0].place_name;
        self.form[which].location.lat = parseFloat(self.suggestions[which][0].center[1]);
        self.form[which].location.lon = parseFloat(self.suggestions[which][0].center[0]);
        self.validateLocation(e);
      },
      removeAutocomplete: function(e) {
        self.hasSuggestions[e.data.which] = false;
      },
      autoFillLocation: function() {
        var self = this;
        self.form.sender.location.name = "One secarooni...";
        var timeoot = setInterval(function() {
          self.form.sender.location.name += ".";
        }, 250);
        navigator.geolocation.getCurrentPosition(function(position) {
          clearInterval(timeoot);
          self.form.sender.location.lat = parseFloat(position.coords.latitude);
          self.form.sender.location.lon = parseFloat(position.coords.longitude);
        });
      },
      getTicker: function() {
        var self = this;
        axios.get('/api/weather_ticker')
        .then(function(resp) {
          self.tickerBlob = resp.data;
          self.emptyTicker = false;
        })
        .catch(function(resp) {
          self.emptyTicker = true;
        });
      },
      getWeatherOpinion: function(code, index) {
        var self = this;
        var opinion;
        if(self.bad.indexOf(code) >= 0) {
          opinion = self.baddies[index];
        } else if(self.good.indexOf(code) >= 0) {
          opinion = self.goodies[index];
        } else {
          opinion = self.riskies[index];
        }
        return opinion;
      },
      shuffle: function(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      },
      shuffleWeatherOpinions: function() {
        var self = this;
        self.baddies = self.shuffle(self.baddies);
        self.riskies = self.shuffle(self.riskies);
        self.goodies = self.shuffle(self.goodies);
      },
      showWeatherWarn: function(code) {
        var self = this;
        return self.bad.indexOf(code) >= 0; 
      },
      toggleWeather: function() {
        var self = this;
        self.showWeather = !self.showWeather;
      },
      postEmail: function(e) {
        e.preventDefault();
        var self = this;
        self.loading = true;
        self.emailButton = "sending...";
        self.form.body = self.form.body.replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);});
        axios.post('/api/e-mail', self.form)
        .then(function(resp) {
          sessionStorage.clear();
          self.loading = false;
          self.done = true;
          self.emailButton = "SENT!";
          window.location.href = "/tracking/" + resp.data.number;
        })
        .catch(function(xhr, type) {
          console.log("RAAAAWWWWWWRRRRRRR");
          console.error("ajax error!");
          console.error(type);
        });
      }
    }
  });
  // auto-expanding textarea support.
  var autoexpanders = document.querySelector('.auto-expand');
  autoexpanders.addEventListener('focus', function(e) {
    var el = e.currentTarget;
    var savedValue = el.value;
    el.value = '';
    el.baseScrollHeight = el.scrollHeight;
    el.lineHeight = parseInt(document.defaultView.getComputedStyle(el, null)["line-height"]);
    el.value = savedValue;
  }, { once: true });
  autoexpanders.addEventListener('input', function(e) {
    var el = e.currentTarget;
    var minRows = el.dataset.minRows | 0;
    var extraRows;
    el.rows = minRows;
    extraRows = Math.ceil((el.scrollHeight - el.baseScrollHeight) / el.lineHeight) - 1;
    el.rows = Math.max(minRows, minRows + extraRows);
    window.scrollTo(0,document.body.scrollHeight);
  });
}


