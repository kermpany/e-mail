if(document.getElementById("e-mail-tracking-section")) {
  var vm = new Vue({
    el: '#e-mail-tracking-section',

    data: {
      loading: true,
      done: false,
      trackingNumber: window.location.pathname.split("/")[2],
      info: {},
      current_location: "",
      showable: false,
      timeToPickUp: "",
      timeToDelivery: "",
      interPoint: [],
      zoomPoint: [],
      origin: null,
      destination: null,
      flightIndex: 0,
      pastRoute: {
        "type": "FeatureCollection",
        "features": [],
      },
      route: {
        "type": "FeatureCollection",
        "features": [],
      },
      futureRoute: {
        "type": "FeatureCollection",
        "features": [],
      },
      point: {},
      lineDistance: null,
      status: "",
      refresher: null,
      counter: 0,
    },
    filters: {
      momentDate: function (date) {
        return moment(date).format('MMM Do');
      },
      momentTime: function (date) {
        let utcDate = moment.tz(date,'UTC');
        let localDate = moment.utc(utcDate).local();

        return localDate.format('h:mm:ssa');
      }
    },

    methods: {
      refreshData: function() {
        // console.log('refreshin at ', moment().utc().format());
        var self = this;
        axios.get('/api/tracking_info/' + self.trackingNumber)
          .then(function(resp) {
            self.info.state = resp.data.state;
            self.info.flights = resp.data.flight_path;
            self.info.events = resp.data.events;

            self.setStatus();
            self.info.progress = self.calculateProgress();
            self.drawRoutes();

            if(self.route.features.length > 0) {
              self.animate();
            }
            if(self.info.state === "delivered") {
              clearInterval(self.refresher);
            }
        })
        .catch(function(error) {
          console.error(error);
        });
      },

      // https://docs.mapbox.com/mapbox-gl-js/example/animate-point-along-route/
      animate: function() {
        var self = this;
        if (self.counter % 4 != 0) {
          self.counter += 1;
          requestAnimationFrame(self.animate);
          return;
        }
        // Update point geometry to a new position based on counter denoting
        // the index to access the arc.
        let newSpot = self.route.features[0].geometry.coordinates[self.counter];
        self.point.features[0].geometry.coordinates = newSpot;

        if (self.pastRoute.features && self.pastRoute.features.length > 0) {
          self.pastRoute.features[self.pastRoute.features.length-1].geometry.coordinates.push(newSpot);
        } else {
            let feature = {
              "type": "Feature",
              "geometry": {
                "type": "LineString",
                "coordinates": [newSpot]
              }
            };
          self.pastRoute.features.push(feature); // = self.route.features;
        }

        self.map.getSource('point').setData(self.point);
        self.map.getSource('pastRoute').setData(self.pastRoute);


        // Request the next frame of animation so long as destination has not
        // been reached.
        if (newSpot && (newSpot[0] !== self.destination[0]) && (self.info.progress * 1000 > self.counter)) {
          requestAnimationFrame(self.animate);
        }
        self.counter += 1;
      },

      setCurrentPosition: function() {
        var self = this;

        if (self.route.features.length > 0) {
          self.currentFeature = self.route.features[0];
          self.lineDistance = turf.lineDistance(self.currentFeature, 'kilometers');
          self.interPoint = turf.along(self.currentFeature, self.info.progress * self.lineDistance, 'kilometers').geometry.coordinates;;
        } else if (self.futureRoute.features.length > 0) {
          self.currentFeature = self.futureRoute.features[0];
          self.lineDistance = turf.lineDistance(self.currentFeature, 'kilometers');
          self.point.features[0].geometry.coordinates = self.currentFeature.geometry.coordinates[0];
          self.interPoint =  self.currentFeature.geometry.coordinates[0];
        } else {
          let lastLeg = self.pastRoute.features.length - 1;
          let finalCoord = self.pastRoute.features[lastLeg].geometry.coordinates.length - 1;
          self.currentFeature = self.pastRoute.features[lastLeg];
          self.lineDistance = turf.lineDistance(self.currentFeature, 'kilometers');
          self.point.features[0].geometry.coordinates = self.currentFeature.geometry.coordinates[finalCoord];
          self.interPoint = self.currentFeature.geometry.coordinates[finalCoord];
        }

        axios.get('https://api.mapbox.com/geocoding/v5/mapbox.places/'+ self.interPoint[0] + ',' + self.interPoint[1] + '.json?access_token=pk.eyJ1IjoiZ2VsaW5rIiwiYSI6ImNpcG55OWk2MDAwMHRoem5iY3draDZvdXQifQ.hpO3gotMwQRjwCiPsWQmAQ')
          .then(function(resp) {
            if(resp && resp.data && resp.data.features) {
              let place = resp.data.features.filter(f => f.place_type.includes('place'));
              // let region = resp.data.features.filter(f => f.place_type.includes('region'));

              if (place.length >= 1) {
                self.current_location = place[0].place_name;
              // I kinda like when it shows the water probably instead of the general region
              // } else if(region.length >= 1) {
              //   self.current_location = region[0].place_name;
              }
            }
          })
          .catch(function(error) {
            console.error(error);
          });
      },

      setStatus: function() {
        var self = this;

        if (self.info.state == "waiting") {
          self.status = 'waiting for next available pickup';
          self.timeToPickUp = self.info.time_posted.from(moment.utc());
        } else if (self.info.state == "processing") {
          self.status = "waiting for next flight";
        } else if (self.info.state == "droning" || self.info.state == "flying") {
          self.status = 'in transit';
          self.timeToDelivery = self.info.eta.fromNow();
          self.timeToPickUp = "";
        } else if (self.info.state == "delivered") {
          self.status = "IT'S DELIVERED!";
        }
        if(self.info.state == "delivered" && self.trackingNumber[0] == "X") {
            self.showable = true;
        }
      },

      setRoutes: function() {
        var self = this;
        self.pastRoute = {
          "type": "FeatureCollection",
          "features": [],
        };
        self.route = {
          "type": "FeatureCollection",
          "features": [],
        };
        self.futureRoute ={
          "type": "FeatureCollection",
          "features": [],
        };
        var partialArc = [];

        self.info.flights.forEach((f,i) => {
          if (f.origin_lat != f.destination_lat && f.origin_lon != f.destination_lon) {
            let feature = {
              "type": "Feature",
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [f.origin_lon, f.origin_lat],
                  [f.destination_lon, f.destination_lat]
                ]
              }
            };
            if(f.has_taken_off && f.has_landed) {
              self.pastRoute.features.push(feature);
            } else if (f.has_taken_off) {
              self.route.features.push(feature)
              self.route.features[0].geometry.coordinates = self.calculateArc(feature);
              self.flightIndex = i;

              // var lineDistance = turf.lineDistance(feature, 'kilometers');
              for (var i = 1; i < 1000; i++) {
                var progress = i / 1000;
                // var segment = turf.along(feature, progress * lineDistance, 'kilometers');
                if (progress < self.info.progress) {
                  partialArc.push(self.route.features[0].geometry.coordinates[i]);
                }
              }
            } else {
              self.futureRoute.features.push(feature);
            }
          }
        });

        // Draw an arc between the `origin` & `destination` of the two points
        self.futureRoute.features.forEach((feature, i) => self.futureRoute.features[i].geometry.coordinates = self.calculateArc(feature));
        self.pastRoute.features.forEach((feature, j) => self.pastRoute.features[j].geometry.coordinates = self.calculateArc(feature));
        if (partialArc.length > 0) {
          self.pastRoute.features.push({
            "type": "Feature",
            "geometry": {
              "type": "LineString",
              "coordinates": partialArc,
            }
          });
        }
      },

      calculateProgress: function() {
        var self = this;
        if (self.status == 'in transit') {
          var flight =  self.info.flights[self.flightIndex];

          let now = moment().utc();
          let flightTime = moment(flight.flight_time).parseZone();
          let timeElapsed = moment.duration(now.diff(flightTime)).as('seconds');
          return timeElapsed / flight.duration;
        }
        return 0;
      },

      calculateArc: function (feature) { 
        var lineDistance = turf.lineDistance(feature, 'kilometers');
        var arc = [turf.along(feature, 0, 'kilometers').geometry.coordinates];
        for (var i = 1; i <= 1000; i++) {
          var segment = turf.along(feature, i / 1000 * lineDistance, 'kilometers');
          arc.push(segment.geometry.coordinates);
        }
        return arc;
      },

      setMapZoom: function (feature) {
        var self = this;

        let coords = feature.geometry.coordinates;

        let zoom_origin = coords[0];
        let zoom_destination = coords[coords.length-1];
        var midPoint = turf.along(feature, .5 * self.lineDistance, 'kilometers').geometry.coordinates;
        var avgPoint = [(zoom_destination[0] + zoom_origin[0]) / 2, (zoom_destination[1] + zoom_origin[1]) / 2];

        var bonusMiles = 0;
        if(zoom_origin[0] * zoom_destination[0] < 0) {
          var distDir1 = Math.abs(zoom_origin[0]) + Math.abs(zoom_destination[0]);
          var distDir2 = 360 - distDir1;
          bonusMiles = Math.min(distDir1, distDir2) == distDir1 ? 0 : 180;
        }

        self.zoomPoint[0] = avgPoint[0] + bonusMiles;
        self.zoomPoint[1] = (midPoint[1] + avgPoint[1])/2;

        var boundingBox = [[0,0],[0,0]]; //sw , ne
        if(zoom_origin[0] < zoom_destination[0]) {
          boundingBox[0][0] = zoom_origin[0] - 4 + bonusMiles;
          boundingBox[1][0] = zoom_destination[0] + 4 + bonusMiles;
        }
        else {
          boundingBox[0][0] = zoom_destination[0] - 4 + bonusMiles;
          boundingBox[1][0] = zoom_origin[0] + 4 + bonusMiles;
        }

        boundingBox[0][1] = Math.min(midPoint[1], Math.min(zoom_origin[1], zoom_destination[1])) - 4;
        boundingBox[1][1] = Math.max(midPoint[1], Math.max(zoom_origin[1], zoom_destination[1])) + 4;

        boundingBox[0][1] = boundingBox[0][1] < -90 ? -90 : boundingBox[0][1];
        boundingBox[1][1] = boundingBox[1][1] > 90 ? 90 : boundingBox[1][1];
        self.boundingBox = boundingBox;
      },

      drawRoutes: function() {
        var self = this;

        self.setRoutes();
        self.setCurrentPosition();
        self.setMapZoom(self.currentFeature);
        self.map.getSource('futureRoute').setData(self.futureRoute);
        self.map.getSource('route').setData(self.route);
        self.map.getSource('pastRoute').setData(self.pastRoute);
        self.map.getSource('point').setData(self.point);
      }
    },

    created: function() {
      var self = this;
      self.loading = true;
      axios.get('/api/tracking_info/' + self.trackingNumber)
      .then(function(resp) {
          var info = resp.data;
          info.time_posted = moment.tz(resp.data.time_posted,'UTC');
          info.time_mailed = moment.tz(resp.data.time_mailed,'UTC');
          info.eta = moment.tz(resp.data.eta,'UTC');
          self.info = info;
          self.info.flights = resp.data.flight_path;
          self.loading = false;
          self.done = true;
          self.setStatus(self.info.state);
          self.setRoutes();
          self.info.progress = self.calculateProgress();

          self.origin = [self.info.origin_lon, self.info.origin_lat];
          self.destination = [self.info.destination_lon, self.info.destination_lat];

          self.point = {
            "type": "FeatureCollection",
            "features": [{
              "type": "Feature",
              "geometry": {
                "type": "Point",
                "coordinates": self.origin
              }
            }]
          };
          self.setCurrentPosition();

          self.setMapZoom(self.currentFeature);

          var zoom = 4.3 - .00032 * self.lineDistance;
          zoom = zoom < 0 ? 0 : zoom;

          mapboxgl.accessToken = 'pk.eyJ1IjoibWNnaW50eSIsImEiOiJjaXhwMjlmdnYwYTVtMzNwdTdzb2xzOWR0In0.gjXgJ256P1CIFUGBtwo4Fg';
          self.map = new mapboxgl.Map({
              container: 'map',
              style: 'mapbox://styles/mcginty/cixqaghqo005z2rqsl6kl0adv',
              center: self.zoomPoint,
              zoom: zoom,
          });
          self.map.fitBounds(self.boundingBox);

          self.map.on('load', function () {
            // Add a source and layer displaying a point which will be animated in a circle.
            self.map.addSource('pastRoute', {
              "type": "geojson",
              "data": self.pastRoute
            });

            self.map.addSource('route', {
              "type": "geojson",
              "data": self.route,
            });

            self.map.addSource('futureRoute', {
              "type": "geojson",
              "data": self.futureRoute
            });

            self.map.addSource('point', {
              "type": "geojson",
              "data": self.point
            });

            self.map.addLayer({
              "id": "pastRoute",
              "source": "pastRoute",
              "type": "line",
              "paint": {
                "line-width": 3,
                "line-color": "#ff2e88"
              }
            });

            self.map.addLayer({
              "id": "route",
              "source": "route",
              "type": "line",
              "paint": {
                "line-width": 3,
                "line-color": "#ff2e88",
                "line-dasharray": [2,3]
              }
            });

            self.map.addLayer({
              "id": "futureRoute",
              "source": "futureRoute",
              "type": "line",
              "paint": {
                "line-width": 3,
                "line-color": "#ffbad8", //"#f099be",
                "line-dasharray": [2,3]
              }
            });

            self.map.addLayer({
              "id": "point",
              "source": "point",
              "type": "symbol",
              "layout": {
                "icon-image": "post-15",
              }
            });

            // Start the animation.
            if(self.route.features.length > 0) {
              self.animate();
            }
            if(self.info.state !== "delivered") {
              self.refresher = setInterval(self.refreshData, 15000);
            }
          });
      })
      .catch(function(error) {
        if (error.response) {
          console.log("Network error", error);
          window.location.href = "/?msg=trbt";
        } else {
          console.log("Error in our logic, caught by axios", error);
        }
      });
    }
  });
}
