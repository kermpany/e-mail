module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-es6-module-transpiler');


  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'static/<%= pkg.name %>.js',
        dest: 'static/<%= pkg.name %>.min.js'
      }
    },

    concat: {
      js: {
        options: {
          banner: "'use strict';\n\n",
          separator: '\n\n'
        },
        src: [
          'static/lib/axios.min.js',
          'static/lib/lodash.custom.min.js',
          'static/lib/vue.min.js',
          // 'static/lib/turf_base.min.js',
          'static/lib/turf.min.js',
          'static/lib/moment.min.js',
          'static/lib/moment-timezone.min.js',
          'tmp/static/js/index.js',
          'tmp/static/js/e-mail.js',
          'tmp/static/js/tracking.js'
        ],
        dest: 'static/<%= pkg.name %>.js',
        nonull: true
      }
    },

    transpile: {
      main: {
      type: "cjs", // or "amd" or "yui"
        files: [{
        expand: true,
        // cwd: 'static/',
        src: [
          'static/js/index.js',
          'static/js/e-mail.js',
          'static/js/tracking.js'
        ],
        dest: 'tmp/'
        }]
      }
    },


    watch: {
      options: {
        interrupt: true,
        atBegin: true
      },
      js: {
        files: ['static/js/**'],
        tasks: ['build_js']
      }
    }
  });

  grunt.registerTask('build_js', ['transpile', 'concat:js', 'uglify']);

  grunt.registerTask('build', ['build_js']);
  // Default task(s).
  grunt.registerTask('default', ['build']);

};
