Holy poo! Somebody just sent you a Kermpany electronic mail!

It's currently on its way through the state-of-the-art Kermpany delivery network.

========
Track your E-mail at https://e-mail.kermpany.com/tracking/{{trackingNumber}}
========

If this is your first "e-mail", we at Kermpany would like to welcome you to the bright new future. E-mails are a lot like regular postcards you would normally get in the mail, except they're delivered entirely in cyberspace.

Well, enough chit chat - see for yourself.

Love,

Kermpany

