use crate::constants::{
  DEG_IN_RADS, DRONE_SPEED, EARTH_RADIUS
};
use crate::datastructs::{Location};
use crate::flights::get_kermp_air;
use chrono::{Datelike, Duration, NaiveDate, NaiveDateTime};
use rand;
use rand::distributions::{Distribution, Uniform};

// #[derive(Debug, serde::Deserialize)]
// struct Feature {
//   center: [f64; 2],
// }

// #[derive(Debug, serde::Deserialize)]
// struct MapboxData {
//   features: [Feature; 2],
// }

// #[derive(Debug, serde::Deserialize)]
// struct PlaceName {
//   place_name: String,
// }

// #[derive(Debug, serde::Deserialize)]
// struct MapboxDataReverse {
//   features: [PlaceName; 1],
// }

pub fn get_drone_travel_duration_in_seconds(distance: f64) -> i32 {
  (distance / DRONE_SPEED * 60. * 60.).floor() as i32 //in seconds
}

pub fn get_next_flight(
  flight_time: NaiveDateTime,
  start_airport: String,
  end_airport: String,
) -> NaiveDateTime {
  let kermp_air = get_kermp_air();

  let ref flights = kermp_air[&start_airport].flights[&end_airport];

  // println!("flights -> {:?}", flights);
  let flight = &flights[0];
  let today =
    NaiveDate::from_ymd_opt(flight_time.year(), flight_time.month(), flight_time.day()).unwrap();
  let comparitor = flight_time + Duration::minutes(15);
  let mut flight_times = flight
    .times
    .iter()
    .filter(|time| today.and_time(**time) > comparitor);

  let flight_tomorrow = today.and_time(flight.times[0]) + Duration::days(1);

  let flight_time = match flight_times.nth(0) {
    Some(time) => today.and_time(*time),
    None => flight_tomorrow,
  };
  flight_time
}

//REVIEW ME PLEASE IM ALMOST CERTAINLY DOING BAD THINGS
pub fn get_closest_airport(lat: f64, lon: f64) -> (Location, f64) {
  // TODO Move these to some config place
  // I TRIED!! IT DIDNT GO WELL
  let locations = vec![
    Location {
        lat: 41.975381,
        lon: -87.901671,
        name: "ORD".into(),
    },
    Location {
        lat: 37.618624,
        lon: -122.379649,
        name: "SFO".into(),
    },
    Location {
        lat: 51.470219,
        lon: -0.454558,
        name: "LHR".into(),
    },
    Location {
        lat: 2.739468,
        lon: 101.705757,
        name: "KLI".into(),
    },
  ];
  let mut dees: [f64; 4] = [0.; 4];

  for i in 0..locations.len() {
    dees[i] = get_birdseye_distance(lat, lon, locations[i].lat, locations[i].lon);
  }

  let mut short_i = 0;
  let mut min_dist = 20000.;
  for i in 0..dees.len() {
    if dees[i] < min_dist {
      min_dist = dees[i];
      short_i = i;
    }
  }

  (locations[short_i].clone(), min_dist)
    // THIS IS ME TRYING
  // let mut dees: [f64; 4] = [0.; 4];

  // for i in 0..AIRPORT_CODES.len()-1 {
  //   dees[i] = get_birdseye_distance(lat, lon, AIRPORT_LOCATIONS[AIRPORT_CODES[i]].lat, AIRPORT_LOCATIONS[AIRPORT_CODES[i]].lon);
  // }


  // let mut short_i = 0;
  // let mut min_dist = 20000.;
  // for i in 0..dees.len() {
  //   if dees[i] < min_dist {
  //     min_dist = dees[i];
  //     short_i = i;
  //   }
  // }

  // (AIRPORT_LOCATIONS[AIRPORT_CODES[short_i]].clone(), min_dist)
}

pub fn get_birdseye_distance(origin_lat: f64, origin_lon: f64, destination_lat: f64, destination_lon: f64) -> f64 {
  let origin_lat_rad: f64 = origin_lat * DEG_IN_RADS;
  let destination_lat_rad: f64 = destination_lat * DEG_IN_RADS;

  let delta_lat: f64 = (destination_lat - origin_lat) * DEG_IN_RADS;
  let delta_lon: f64 = (destination_lon - origin_lon) * DEG_IN_RADS;

  let a: f64 = ((delta_lat / 2.).sin() * (delta_lat / 2.).sin())
    + (origin_lat_rad.cos() * destination_lat_rad.cos() * (delta_lon / 2.).sin() * (delta_lon / 2.).sin());
  let c: f64 = 2. * (a.sqrt()).atan2((1f64 - a).sqrt());
  EARTH_RADIUS * c
}

// pub fn get_current_location_coords(
//   slat: f64,
//   slon: f64,
//   rlat: f64,
//   rlon: f64,
//   time_sent: i64,
//   time_now: i64,
// ) -> (f64, f64, f64) {
//   let total_distance = get_birdseye_distance(slat, slon, rlat, rlon);

//   let time_elapsed = time_now - time_sent;

//   if time_elapsed <= 0 {
//     return (slat, slon, 0.);
//   }

//   let distance_traveled = ((time_elapsed / 60 / 60) * 210) as f64; //speed in km/hour

//   let slat_rad: f64 = slat * DEG_IN_RADS;
//   let rlat_rad: f64 = rlat * DEG_IN_RADS;
//   let slon_rad: f64 = slon * DEG_IN_RADS;
//   let rlon_rad: f64 = rlon * DEG_IN_RADS;

//   let sperm = distance_traveled / EARTH_RADIUS;
//   let frac = distance_traveled / total_distance;

//   let a = ((1. - frac) * sperm).sin() / sperm.sin(); // stackoverflow used 0*sperm for first thing
//   let b = (frac * sperm).sin() / sperm.sin();

//   let x = a * slat_rad.cos() * slon_rad.cos() + b * rlat_rad.cos() * rlon_rad.cos();

//   let y = a * slat_rad.cos() * slon_rad.sin() + b * rlat_rad.cos() * rlon_rad.sin();

//   let zz = a * slat_rad.sin() + b * rlat_rad.sin();

//   let clat = zz.atan2((x * x + y * y).sqrt()) / DEG_IN_RADS;
//   let clon = y.atan2(x) / DEG_IN_RADS;

//   (clat, clon, frac)
// }

pub fn generate_trackings() -> (String, String) {
  let mut rng = rand::thread_rng();
  let num_range = Uniform::new(48u8, 58u8); // '0' - '9'
  let alpha_range = Uniform::new(65u8, 91u8); // 'A' - 'Z'
  let mut s_tracking = String::with_capacity(12);
  let mut r_tracking = String::with_capacity(12);
  s_tracking.push('K');
  r_tracking.push('X');
  for _ in 0..2 {
    s_tracking.push(alpha_range.sample(&mut rng) as char);
    r_tracking.push(alpha_range.sample(&mut rng) as char);
  }
  for _ in 0..10 {
    s_tracking.push(num_range.sample(&mut rng) as char);
    r_tracking.push(num_range.sample(&mut rng) as char);
  }
  s_tracking.push(alpha_range.sample(&mut rng) as char);
  r_tracking.push(alpha_range.sample(&mut rng) as char);

  (s_tracking, r_tracking)
}

pub fn generate_kermpdrone_id() -> String {
  let mut rng = rand::thread_rng();
  let num_range = Uniform::new(48u8, 58u8); // '0' - '9'

  let mut drone_id = "KermpDrone".to_string();

  for _ in 0..7 {
    drone_id.push(num_range.sample(&mut rng) as char);
  }
  drone_id
}

#[test]
fn test_get_birdseye_distance() {
  assert_eq!(157.24938127194397, get_birdseye_distance(0., 0., 1., 1.));
}

// #[test]
// fn test_get_current_location_coords() {
//   //assert_eq!((41.98510511645927, -96.5793085496892), get_current_location_coords(40.7486, -73.9864, 37.768894, -122.452082, 1_483_963_200, 1_483_998_624));
// }
