use crate::datastructs::Location;
use regex::Regex;
use std::collections::HashMap;

pub const DRONE_SPEED: f64 = 210.; // km/h
pub const DEG_IN_RADS: f64 = 3.14159265358979323846264338327950288 / 180.;
pub const EARTH_RADIUS: f64 = 6371.; // in kilometers
pub const EMAIL_RETRIES: usize = 5;
pub const EMAIL_RETRY_SLEEP_SECS: u64 = 15;

lazy_static! {
	pub static ref TRACKING_NUMBER_REGEX: Regex =
		Regex::new(r"[[:upper:]]{3}\d{10}[[:upper:]]").unwrap();

  pub static ref AIRPORT_CODES: Vec<&'static str> = vec![
    "ORD",
    "SFO",
    "LHR",
    "KLI",
  ];

	pub static ref LOCATION_WEATHER_CODES: HashMap<&'static str, usize> = {
		let mut m = HashMap::new();
		m.insert("ORD", 4887398);
		m.insert("SFO", 5391959);
		m.insert("LHR", 2643743);
		m.insert("KLI", 1733046);
		m
	};
  pub static ref AIRPORT_LOCATIONS: HashMap<&'static str, Location> = {
    let mut m = HashMap::new();
    m.insert("ORD",
      Location {
        lat: 41.975381,
        lon: -87.901671,
        name: "ORD".into(),
      });
    m.insert("SFO",
      Location {
        lat: 37.618624,
        lon: -122.379649,
        name: "SFO".into(),
      });
    m.insert("LHR",
      Location {
        lat: 51.470219,
        lon: -0.454558,
        name: "LHR".into(),
      });
    m.insert("KLI",
      Location {
        lat: 2.739468,
        lon: 101.705757,
        name: "KLI".into(),
      });
      m
  };
}

pub const BAD_WEATHER_CODES: &'static [usize] = &[
  202, 211, 212, 221, 232, 502, 503, 504, 522, 531, 612, 622, 711, 731, 762, 78,
];
// pub const KLI_FLIGHTS: &'static [&'static str; 2] = &["kl700", "lon415"];
// pub const SFO_FLIGHTS: &'static [&'static str; 4] = &["sfo650", "sfo700", "chi890", "lon890"];
// pub const CHI_FLIGHTS: &'static [&'static str; 4] = &["chi700", "chi890", "sfo650", "lon650"];
// pub const LHR_FLIGHTS: &'static [&'static str; 6] = &["lon415", "lon650", "lon890", "kl700", "sfo700", "chi700"];

pub const EVENT_TYPE_DELIVERED: &'static str = "delivered";
pub const EVENT_TYPE_PROCESSING: &'static str = "processing";
pub const EVENT_TYPE_FLIGHT_TRANSIT: &'static str = "flying";
pub const EVENT_TYPE_DRONE_TRANSIT: &'static str = "droning";
pub const EVENT_TYPE_WAITING: &'static str = "waiting";
