use crate::constants::{
  AIRPORT_LOCATIONS,
  EVENT_TYPE_DRONE_TRANSIT, EVENT_TYPE_FLIGHT_TRANSIT,
  EVENT_TYPE_DELIVERED, EVENT_TYPE_PROCESSING, EVENT_TYPE_WAITING
};
use crate::datastructs::{EmailRequest, Location, ThreadInfo, TrackingInfo};
use crate::error::internal_error;
use crate::flights::get_kermp_air;
use crate::helpers;
use crate::models::{DeliveryEvent, EMail, Flight, NewDeliveryEvent, NewEMail, NewFlight, NewThread, Thread};
use crate::weather::WeatherResponse;

use axum::http::StatusCode;
use chrono::{Datelike, DateTime, Duration, NaiveDate, Utc};
use diesel::{
  r2d2::{ConnectionManager, Pool, PooledConnection},
  BoolExpressionMethods, ExpressionMethods, PgArrayExpressionMethods, PgConnection, PgTextExpressionMethods, QueryDsl,
  RunQueryDsl,
};
use std::sync::{Arc, RwLock};

lazy_static! {
  static ref WEDDE: Arc<RwLock<Vec<WeatherResponse>>> = Arc::new(RwLock::new(vec![]));
}

pub type DbPool = Pool<ConnectionManager<PgConnection>>;
pub type DbConn = PooledConnection<ConnectionManager<PgConnection>>;

pub fn mark_mail_delivered(pool: DbPool, row_id: i32) -> Result<(), (StatusCode, String)> {
  use crate::schema::e_mails::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  diesel::update(e_mails.filter(id.eq(row_id)))
    .set(delivery_email.eq(true))
    .execute(&mut conn)
    .map_err(internal_error)?;

  Ok(())
}

pub fn get_new_delivered_emails(pool: DbPool) -> Result<Vec<EMail>, (StatusCode, String)> {
  use crate::schema::e_mails::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  Ok(
    e_mails
      .filter(delivery_email.eq(false).and(arrival_time.is_not_null()))
      .get_results::<EMail>(&mut conn)
      .map_err(internal_error)?,
  )
}

pub fn grab_tracking_data(
  pool: DbPool,
  tracking: String,
) -> Result<TrackingInfo, (StatusCode, String)> {
  use crate::schema::delivery_events::dsl as delivery_events_schema;
  use crate::schema::e_mails::dsl::*;
  use crate::schema::flights::dsl as flight_schema;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let results = e_mails
    .filter(
      recipient_tracking
        .eq(&tracking)
        .or(sender_tracking.eq(&tracking)),
    )
    .limit(1)
    .load::<EMail>(&mut conn)
    .map_err(internal_error)?;

  if results.is_empty() {
    return Err((
      StatusCode::BAD_REQUEST,
      "tracking number doesn't exist".to_string(),
    ));
  }

  let result = &results[0];

  let utc: DateTime<Utc> = Utc::now();

  let slat = result.sender_lat;
  let slon = result.sender_lon;
  let rlat = result.recipient_lat;
  let rlon = result.recipient_lon;
  let semail = result.sender_email.clone();
  let remail = result.recipient_email.clone();
  let mail_time = result.mailed_time;
  let post_time = result.posted_time;
  let eta = result.estimated_arrival_time;
  let sloc = result.sender_location.clone();
  let rloc = result.recipient_location.clone();

  let naive_time_now = utc.naive_utc();

  let events: Vec<DeliveryEvent> = delivery_events_schema::delivery_events
    .filter(delivery_events_schema::email_id.eq(result.id).and(delivery_events_schema::event_time.lt(naive_time_now)))
    .order(delivery_events_schema::event_time.asc())
    .get_results(&mut conn)
    .map_err(internal_error)?;
  let flight_path: Vec<Flight> = flight_schema::flights
    .filter(flight_schema::emails_on_board.contains(vec![result.id]))
    .order(flight_schema::flight_time.asc())
    .get_results(&mut conn)
    .map_err(internal_error)?;

  let state = match events.last() {
    Some(e) => e.event_type.clone(),
    None => "borked".to_string(),
  };

  Ok(TrackingInfo {
    state,
    origin: sloc,
    origin_lat: slat,
    origin_lon: slon,
    destination: rloc,
    destination_lat: rlat,
    destination_lon: rlon,
    progress: 0.1, // TODO: actually calculate back here (done in tracking.js)
    time_mailed: mail_time,
    time_posted: post_time,
    eta,
    flight_path,
    events,
    sender_email: semail,
    recipient_email: remail,
  })
}

pub async fn get_e_mail(
  pool: DbPool,
  tracking: String,
) -> Result<ThreadInfo, (StatusCode, String)> {
  use crate::schema::e_mail_threads::dsl as thread_schema;
  use crate::schema::e_mails::dsl as e_mail_schema;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let result =
    diesel::update(e_mail_schema::e_mails.filter(e_mail_schema::recipient_tracking.eq(&tracking)))
      .set(e_mail_schema::opened.eq(true))
      .get_result::<EMail>(&mut conn)
      .map_err(internal_error)?;

  let prev_trackings = match result.thread {
    Some(_) => {
      let prev_tracking_result = thread_schema::e_mail_threads
        .filter(thread_schema::id.eq(result.thread.unwrap()))
        .get_result::<Thread>(&mut conn);

      match prev_tracking_result {
        Ok(t) => t.tracking_nums,
        Err(_) => vec![],
      }
    }
    None => vec![],
  };

  let now = Utc::now().naive_utc();

  let info = grab_tracking_data(pool.clone(), tracking)?;

  match result.arrival_time {
    Some(time) => {
      if time > now {
        Err((
          StatusCode::BAD_REQUEST,
          "the e-mail hasn't arrived yet!".to_string(),
        ))
      } else {
        Ok(ThreadInfo {
          tracking_info: info,
          letter: result.e_mail_body,
          letters: prev_trackings,
        })
      }
    }
    None => Err((
      StatusCode::BAD_REQUEST,
      "the e-mail hasn't arrived yet! (and we don't know when it will)".to_string(),
    )),
  }
}

pub fn create_new_thread(pool: DbPool, tracking: String) -> i32 {
  use crate::schema::e_mail_threads::dsl::*;

  let mut conn: DbConn = pool.get().unwrap();

  let tracking_arr = vec![tracking];

  let new_thread = NewThread {
    tracking_nums: tracking_arr,
  };

  diesel::insert_into(e_mail_threads)
    .values(&new_thread)
    .get_result::<Thread>(&mut conn)
    .map_err(internal_error)
    .unwrap()
    .id
}

pub fn update_thread(pool: DbPool, prev_tracking: String, tracking: String) -> i32 {
  use crate::schema::e_mail_threads::dsl as thread_schema;
  use crate::schema::e_mails::dsl as e_mail_schema;

  // TODO change update(blaaaah).unwrap() to plain update
  // have fn return Result<i32>
  let mut conn: DbConn = pool.get().unwrap();

  let e_mail_thread: ::std::result::Result<Thread, _> = thread_schema::e_mail_threads
    .filter(thread_schema::tracking_nums.contains(vec![prev_tracking.clone()]))
    .get_result(&mut conn);

  let (thread_id, mut tracking_arr) = match e_mail_thread {
    Ok(t) => (t.id, t.tracking_nums),
    Err(_) => {
      let tid = create_new_thread(pool.clone(), prev_tracking.clone());
      diesel::update(
        e_mail_schema::e_mails.filter(e_mail_schema::recipient_tracking.eq(prev_tracking.clone())),
      )
      .set(e_mail_schema::thread.eq(tid))
      .execute(&mut conn)
      .unwrap();
      (tid, vec![prev_tracking.clone()])
    }
  };

  tracking_arr.push(tracking);

  diesel::update(thread_schema::e_mail_threads.filter(thread_schema::id.eq(thread_id)))
    .set(thread_schema::tracking_nums.eq(tracking_arr))
    .execute(&mut conn)
    .unwrap();

  thread_id
}

pub fn save_edashmail(pool: DbPool, email_req: &EmailRequest) -> Result<EMail, (StatusCode, String)> {
  use crate::schema::e_mails::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let (stracking, rtracking) = helpers::generate_trackings();

  let (sloc, rloc) = (&email_req.sender.location, &email_req.recipient.location);

  let mail_time = Utc::now().naive_utc();

  let thread_id = match email_req.prev_tracking {
    Some(ref prev_tracking) => {
      update_thread(pool.clone(), prev_tracking.clone(), rtracking.clone())
    }
    None => create_new_thread(pool.clone(), rtracking.clone()),
  };

  let new_e_mail = NewEMail {
    sender_email: &email_req.sender.email,
    sender_lat: sloc.lat,
    sender_lon: sloc.lon,
    sender_location: &email_req.sender.location.name,
    sender_tracking: &stracking,
    recipient_email: &email_req.recipient.email,
    recipient_lat: rloc.lat,
    recipient_lon: rloc.lon,
    recipient_location: &email_req.recipient.location.name,
    recipient_tracking: &rtracking,
    mailed_time: mail_time,
    posted_time: mail_time, // setting below
    e_mail_body: &email_req.body,
    thread: thread_id,
    estimated_arrival_time: mail_time, // getting the real calc in create_delivery_path
  };

  let e_mail: EMail = diesel::insert_into(e_mails)
    .values(&new_e_mail)
    .get_result(&mut conn)
    .map_err(internal_error)?;

  Ok(e_mail)
}

pub fn create_delivery_path(pool: DbPool, e_mail_id: i32) -> Result<(),  (StatusCode, String)> {
  use crate::schema::e_mails::dsl as e_mail_schema;
  use crate::schema::delivery_events::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let e_mail = e_mail_schema::e_mails.find(e_mail_id)
    .first::<EMail>(&mut conn)
    .map_err(internal_error)?;

  let origin = Location {
    lat: e_mail.sender_lat,
    lon: e_mail.sender_lon,
    name: e_mail.sender_location,
  };
  let destination = Location {
    lat: e_mail.recipient_lat,
    lon: e_mail.recipient_lon,
    name: e_mail.recipient_location,
  };

  // time it takes to go from origin to closest airport
  let (origin_airport, km_to_origin_airport) = helpers::get_closest_airport(origin.lat, origin.lon);
  let (destination_airport, km_to_destination_airport) = helpers::get_closest_airport(destination.lat, destination.lon);
  let origin_drone_duration_secs = helpers::get_drone_travel_duration_in_seconds(km_to_origin_airport);
  let destination_drone_duration_secs = helpers::get_drone_travel_duration_in_seconds(km_to_destination_airport);
  
  let origin_drone_duration = Duration::seconds(origin_drone_duration_secs as i64);
  let destination_drone_duration = Duration::seconds(destination_drone_duration_secs as i64);

  let drone_id = helpers::generate_kermpdrone_id();

  let mut new_flights = vec![];
  new_flights.push(NewFlight {
    flight_id: drone_id.clone(),
    flight_time: e_mail.mailed_time + origin_drone_duration,
    emails_on_board: [e_mail_id].to_vec(),
    origin: origin.name,
    destination: origin_airport.name.clone(),
    origin_lat: origin.lat,
    origin_lon: origin.lon,
    destination_lat: origin_airport.lat,
    destination_lon: origin_airport.lon,
    duration: origin_drone_duration_secs,
  });
  
  let drone_swoop_time = e_mail.mailed_time + origin_drone_duration;

  diesel::insert_into(delivery_events)
    .values(NewDeliveryEvent {
      email_id: e_mail_id,
      event_text: format!("Waiting for {}", drone_id),
      event_time: e_mail.mailed_time,
      event_type: EVENT_TYPE_WAITING.into(),
    })
    .execute(&mut conn)
    .map_err(internal_error)?;

  let final_drone_flight_time;
  if origin_airport.name.clone() == destination_airport.name.clone() {
    final_drone_flight_time =  e_mail.mailed_time + origin_drone_duration + destination_drone_duration;
  } else {
    let mut travel_start_time = e_mail.mailed_time + origin_drone_duration + origin_drone_duration;
    let kermp_air = get_kermp_air();

    let ref flights = kermp_air[&origin_airport.name].flights[&destination_airport.name];

    let mut flight_from = &AIRPORT_LOCATIONS[origin_airport.name.as_str()];
    let mut flight_to;
    let mut final_flight_time = travel_start_time;

    for flight in flights {
      flight_to = &AIRPORT_LOCATIONS[flight.stop.as_str()];
      let today = NaiveDate::from_ymd_opt(
        travel_start_time.year(),
        travel_start_time.month(),
        travel_start_time.day(),
      )
      .unwrap();
      let comparitor = travel_start_time;
      let mut flight_times = flight
        .times
        .iter()
        .filter(|time| today.and_time(**time) > comparitor);

      let flight_tomorrow = today.and_time(flight.times[0]) + Duration::days(1);

      let flight_time = match flight_times.nth(0) {
        Some(time) => today.and_time(*time),
        None => flight_tomorrow,
      };
      travel_start_time = flight_time + Duration::seconds(flight.duration.into());

      new_flights.push(NewFlight {
        flight_id: flight.flight_id.clone(),
        flight_time,
        emails_on_board: [e_mail_id].to_vec(),
        origin: flight_from.name.clone(),
        destination: flight_to.name.clone(),
        origin_lat: flight_from.lat,
        origin_lon: flight_from.lon,
        destination_lat: flight_to.lat,
        destination_lon: flight_to.lon,
        duration: flight.duration,
      });
      flight_from = flight_to;

      final_flight_time = travel_start_time;
    }

    final_drone_flight_time = final_flight_time + destination_drone_duration;
  }

  new_flights.push(NewFlight {
    flight_id: helpers::generate_kermpdrone_id(),
    flight_time: final_drone_flight_time,
    emails_on_board: [e_mail_id].to_vec(),
    origin: destination_airport.name,
    destination: destination.name,
    origin_lat: destination_airport.lat,
    origin_lon: destination_airport.lon,
    destination_lat: destination.lat,
    destination_lon: destination.lon,
    duration: destination_drone_duration_secs,
  });

  let _ = upsert_flights(pool.clone(), new_flights).map_err(|_| ());
  let _ = diesel::update(e_mail_schema::e_mails.filter(e_mail_schema::id.eq(e_mail_id)))
    .set((
      e_mail_schema::estimated_arrival_time.eq(final_drone_flight_time + destination_drone_duration),
      e_mail_schema::posted_time.eq(drone_swoop_time)
    ))
    .get_results::<EMail>(&mut conn)
    .map_err(internal_error)?;
  Ok(())
}

pub fn new_flights_taking_off(
  pool: DbPool,
  fliables: Vec<String>,
) -> Result<(), (StatusCode, String)> {
  use crate::schema::delivery_events::dsl::*;
  use crate::schema::flights::dsl as flight_schema;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;
  let now = Utc::now().naive_utc();

  // update flights that have taken off at now()
  let results = diesel::update(
    flight_schema::flights.filter(
      flight_schema::origin
        .eq_any(fliables)
        .and(flight_schema::destination.eq_any(vec!["KLI", "ORD", "LHR", "SFO"]))
        .and(
          flight_schema::flight_time
            .lt(now)
            .and(flight_schema::delayed_to.is_null())
            .or(flight_schema::delayed_to.lt(now)),
        )
        .and(flight_schema::has_taken_off.eq(false)),
    ),
  )
  .set(flight_schema::has_taken_off.eq(true))
  .get_results::<Flight>(&mut conn)
  .map_err(internal_error)?;

  for r in results {
    // add land time to newly taken off flights
    diesel::update(flight_schema::flights.filter(flight_schema::id.eq(r.id)))
      .set(flight_schema::flight_land_time.eq(r.flight_time + Duration::seconds(r.duration.into())))
      .execute(&mut conn)
      .map_err(internal_error)?;

    // create delivery events for mail on board
    for e in r.emails_on_board {
      diesel::insert_into(delivery_events)
        .values(NewDeliveryEvent {
          email_id: e,
          event_text: format!("In transit from {} to {}", r.origin, r.destination),
          event_time: now,
          event_type: EVENT_TYPE_FLIGHT_TRANSIT.into(),
        })
        .execute(&mut conn)
        .map_err(internal_error)?;
    }
  }

  Ok(())
}

pub fn new_drones_taking_off(
  pool: DbPool,
) -> Result<(), (StatusCode, String)> {
  use crate::schema::delivery_events::dsl as delivery_events_schema;
  // use crate::schema::e_mails::dsl::*;
  use crate::schema::flights::dsl:: *;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;
  let now = Utc::now().naive_utc();

  // update flights that have taken off at now()
  let results = diesel::update(
    flights.filter(
      flight_id.ilike(
        "%KermpDrone%"
      ).and(
        flight_time.lt(now)
        .and(delayed_to.is_null())
        .or(delayed_to.lt(now))
      ).and(
        has_taken_off.eq(false)
      )
    )
  )
  .set(has_taken_off.eq(true))
  .get_results::<Flight>(&mut conn)
  .map_err(internal_error)?;

  for r in results {
    // add land time to newly taken off flights
    diesel::update(flights.filter(id.eq(r.id)))
      .set(flight_land_time.eq(r.flight_time + Duration::seconds(r.duration.into())))
      .execute(&mut conn)
      .map_err(internal_error)?;

    // create delivery events for mail on board
    for e in r.emails_on_board {
      diesel::insert_into(delivery_events_schema::delivery_events)
        .values(NewDeliveryEvent {
          email_id: e,
          event_text: format!("In transit from {} to {}", r.origin, r.destination),
          event_time: now,
          event_type: EVENT_TYPE_DRONE_TRANSIT.into(),
        })
        .execute(&mut conn)
        .map_err(internal_error)?;
    }
  }

  Ok(())
}

pub fn new_flights_landing(pool: DbPool) -> Result<(), (StatusCode, String)> {
  use crate::schema::delivery_events::dsl::*;
  use crate::schema::flights::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let now = Utc::now().naive_utc();

  // update flights that have landed at now()
  let results = diesel::update(
    flights.filter(
      origin.eq_any(vec!["LHR","KLI","ORD","SFO"])
      .and(
        destination.eq_any(vec!["LHR","KLI","ORD","SFO"])
      ).and(
        flight_land_time
          .is_not_null()
          .and(flight_land_time.lt(now))
          .and(has_landed.eq(false)),
    )),
  )
  .set(has_landed.eq(true))
  .get_results::<Flight>(&mut conn)
  .map_err(internal_error)?;

  for r in results {
    for e in r.emails_on_board {
      diesel::insert_into(delivery_events)
        .values(vec![
          NewDeliveryEvent {
            email_id: e,
            event_text: format!("Landed at {}", r.destination),
            event_time: now,
            event_type: EVENT_TYPE_PROCESSING.into(),
          }
        ])
        .execute(&mut conn)
        .map_err(internal_error)?;
    }
  }
  Ok(())
}

pub fn new_drones_landing(pool: DbPool) -> Result<(), (StatusCode, String)> {
  use crate::schema::delivery_events::dsl::*;
  use crate::schema::flights::dsl::*;
  use crate::schema::e_mails::dsl as e_mail_schema;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let now = Utc::now().naive_utc();

  // update drones that have landed at now()
  let results = diesel::update(
    flights.filter(
      flight_id.ilike(
        "%KermpDrone%"
      ).and(
        flight_land_time
          .is_not_null()
          .and(flight_land_time.lt(now))
          .and(has_landed.eq(false))
      )
    )
  )
  .set(has_landed.eq(true))
  .get_results::<Flight>(&mut conn)
  .map_err(internal_error)?;

  for r in results {
    for e in r.emails_on_board {
      let email = e_mail_schema::e_mails.find(e)
        .first::<EMail>(&mut conn)
        .map_err(internal_error)?;

      let (text, event) = if r.destination == email.recipient_location {
        diesel::update(e_mail_schema::e_mails.filter(e_mail_schema::id.eq(e)))
          .set(e_mail_schema::arrival_time.eq(now))
          .execute(&mut conn)
          .map_err(internal_error)?;

        (format!("Delivered!"), EVENT_TYPE_DELIVERED.into())
      } else {
        (format!("Landed at {0}", r.destination), EVENT_TYPE_PROCESSING.into())
      };

      diesel::insert_into(delivery_events)
        .values(NewDeliveryEvent {
          email_id: e,
          event_text: text,
          event_time: now,
          event_type: event,
        })
        .execute(&mut conn)
        .map_err(internal_error)?;
    }
  }
  Ok(())
}

pub fn upsert_flights(
  pool: DbPool,
  new_flights: Vec<NewFlight>,
) -> Result<i32, (StatusCode, String)> {
  use crate::schema::flights::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  let _ = diesel::insert_into(flights).values(flights);
  // TODO(g): should the below actually be what happens here?
  // diesel::insert_into(flights)
  //     .values(flights)
  //     .execute(&mut conn)?;
  for new_flight in new_flights {
    let _something = diesel::sql_query(
      format!("INSERT INTO flights (flight_id, flight_time, emails_on_board, origin, destination, origin_lat, origin_lon, destination_lat, destination_lon, duration)
          VALUES ( '{}', '{}', ARRAY[{}], '{}', '{}', '{}', '{}', '{}', '{}', '{}' )
          ON CONFLICT ON CONSTRAINT unique_flight
              DO
                  UPDATE SET emails_on_board = array_append(flights.emails_on_board, {});", new_flight.flight_id, new_flight.flight_time, new_flight.emails_on_board[0], new_flight.origin, new_flight.destination, new_flight.origin_lat, new_flight.origin_lon, new_flight.destination_lat, new_flight.destination_lon, new_flight.duration, new_flight.emails_on_board[0]))
    .load::<Flight>(&mut conn)
    .map_err(internal_error)?;
  }
  Ok(1)
}

pub fn update_affected_flight(
  pool: DbPool,
  airport: &WeatherResponse,
) -> Result<(), (StatusCode, String)> {
  use crate::schema::flights::dsl::*;

  let mut conn: DbConn = pool.get().map_err(internal_error)?;

  // get all rows that need a date update
  let delay_region_start = Utc::now().naive_utc() + Duration::minutes(2);
  let delay_region_end = Utc::now().naive_utc() + Duration::minutes(64);
  let airport_code = airport.short_name.clone().unwrap();
  let delayed_flights = flights
    .filter(
      origin
        .eq(airport_code)
        .and(flight_time.between(delay_region_start, delay_region_end))
        .and(has_taken_off.eq(false)),
    )
    .load::<Flight>(&mut conn)
    .map_err(internal_error)?;

  for flight in delayed_flights {
    let delay = match flight.delayed_to {
      Some(time) => time + Duration::minutes(15),
      None => flight.flight_time + Duration::minutes(15),
    };

    diesel::update(flights.filter(id.eq(flight.id)))
      .set(delayed_to.eq(delay))
      .execute(&mut conn)
      .map_err(internal_error)?;

    // get the other flights with same emails on them
    let mut new_flights = vec![];
    for e_mail_id in flight.emails_on_board {
      let connected_flights = flights
        .filter(
          id.ne(flight.id).and(
            emails_on_board
              .contains(vec![e_mail_id])
              .and(flight_time.lt(delay + Duration::seconds(flight.duration.into()))),
          ),
        )
        .get_results::<Flight>(&mut conn)
        .map_err(internal_error)?;

      // println!("connected flights {:?}", connected_flights);
      // doing a bad number of looops here. should revise the add to flights function
      // and just start over. but need to know that it's at an airport. so dont drone at all
      // this is a very known runtime because it's only ever gonna be 1 extra flight so shrug
      for connection in connected_flights {
        // println!("old on board {:?}", connection.emails_on_board);
        let mut new_on_board = connection.emails_on_board;
        let i = new_on_board.iter().position(|x| *x == e_mail_id).unwrap();
        new_on_board.remove(i);
        // println!("new on board {:?}", new_on_board);
        diesel::update(flights.filter(id.eq(connection.id)))
          .set(emails_on_board.eq(new_on_board))
          .execute(&mut conn)
          .map_err(internal_error)?;

        new_flights.push(NewFlight {
          flight_id: connection.flight_id.clone(),
          flight_time: helpers::get_next_flight(
            connection.flight_time.clone(),
            connection.origin.clone(),
            connection.destination.clone(),
          ), // next flight at this airport
          emails_on_board: [e_mail_id].to_vec(),
          origin: connection.origin.clone(),
          destination: connection.destination.clone(),
          origin_lat: connection.origin_lat,
          origin_lon: connection.origin_lon,
          destination_lat: connection.destination_lat,
          destination_lon: connection.destination_lon,
          duration: flight.duration,
        });
      }
    }
    // println!("new flights {:?}", new_flights);
    let _ = upsert_flights(pool.clone(), new_flights)?;
    // if take off time is now after arrival time
    // move e_mail to new flight
  }

  Ok(())
}

pub fn update_ticker(blob: Vec<WeatherResponse>) {
  *WEDDE.write().unwrap() = blob;
}

pub fn get_ticker_data() -> Vec<WeatherResponse> {
  WEDDE.read().unwrap().clone()
}
