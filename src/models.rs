use crate::schema::{delivery_events, e_mail_threads, e_mails, flights};

use chrono::NaiveDateTime;
use diesel::prelude::*;

#[derive(serde::Serialize, Selectable, Queryable, Debug)]
#[diesel(table_name = e_mails)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct EMail {
  pub id: i32,
  pub sender_email: String,
  pub sender_lat: f64,
  pub sender_lon: f64,
  pub sender_location: String,
  pub sender_tracking: String,
  pub recipient_email: String,
  pub recipient_lat: f64,
  pub recipient_lon: f64,
  pub recipient_location: String,
  pub recipient_tracking: String,
  pub mailed_time: NaiveDateTime,
  pub posted_time: NaiveDateTime,
  pub arrival_time: Option<NaiveDateTime>,
  pub e_mail_body: String,
  pub opened: bool,
  pub delivery_email: bool,
  pub thread: Option<i32>,
  pub estimated_arrival_time: NaiveDateTime,
}

#[derive(serde::Deserialize, Insertable)]
#[diesel(table_name = e_mails)]
pub struct NewEMail<'a> {
  pub sender_email: &'a str,
  pub sender_lat: f64,
  pub sender_lon: f64,
  pub sender_location: &'a str,
  pub sender_tracking: &'a str,
  pub recipient_email: &'a str,
  pub recipient_lat: f64,
  pub recipient_lon: f64,
  pub recipient_location: &'a str,
  pub recipient_tracking: &'a str,
  pub mailed_time: NaiveDateTime,
  pub posted_time: NaiveDateTime,
  pub e_mail_body: &'a str,
  pub thread: i32,
  pub estimated_arrival_time: NaiveDateTime,
}

#[derive(serde::Serialize, Queryable, Selectable)]
#[diesel(table_name = e_mail_threads)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Thread {
  pub id: i32,
  pub tracking_nums: Vec<String>,
}

#[derive(Insertable)]
#[diesel(table_name = e_mail_threads)]
pub struct NewThread {
  pub tracking_nums: Vec<String>,
}

#[derive(Queryable, serde::Serialize, AsChangeset, Identifiable, Debug)]
#[diesel(table_name = delivery_events)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct DeliveryEvent {
  pub id: i32,
  pub email_id: i32,
  pub event_text: String,
  pub event_time: NaiveDateTime,
  pub event_type: String,
}

#[derive(Insertable)]
#[diesel(table_name = delivery_events)]
pub struct NewDeliveryEvent {
  pub email_id: i32,
  pub event_text: String,
  pub event_time: NaiveDateTime,
  pub event_type: String,
}

impl NewDeliveryEvent {
  pub fn from_event_data(email_id: i32, data: &EventData) -> NewDeliveryEvent {
    NewDeliveryEvent {
      email_id,
      event_text: data.event_text.clone(),
      event_time: data.event_time.clone(),
      event_type: data.event_type.clone(),
    }
  }
}

pub struct EventData {
  pub event_type: String,
  pub event_text: String,
  pub event_time: NaiveDateTime,
}

#[derive(Debug, Queryable, QueryableByName, serde::Serialize, AsChangeset, Identifiable)]
#[diesel(table_name = flights)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Flight {
  pub id: i32,
  pub flight_id: String,
  pub flight_time: NaiveDateTime,
  pub delayed_to: Option<NaiveDateTime>,
  pub emails_on_board: Vec<i32>,
  pub origin: String,
  pub destination: String,
  pub connecting_flights: Vec<i32>,
  pub has_taken_off: bool,
  pub has_landed: bool,
  pub duration: i32,
  pub flight_land_time: Option<NaiveDateTime>,
  pub origin_lat: f64,
  pub origin_lon: f64,
  pub destination_lat: f64,
  pub destination_lon: f64,
}

#[derive(Insertable)]
#[diesel(table_name = flights)]
pub struct NewFlight {
  pub flight_id: String,
  pub flight_time: NaiveDateTime,
  pub emails_on_board: Vec<i32>,
  pub origin: String,
  pub destination: String,
  pub duration: i32,
  pub origin_lat: f64,
  pub origin_lon: f64,
  pub destination_lat: f64,
  pub destination_lon: f64,
}