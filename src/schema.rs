// @generated automatically by Diesel CLI.

diesel::table! {
    delivery_events (id) {
        id -> Int4,
        email_id -> Int4,
        event_text -> Text,
        event_time -> Timestamp,
        event_type -> Text,
    }
}

diesel::table! {
    e_mail_threads (id) {
        id -> Int4,
        tracking_nums -> Array<Text>,
    }
}

diesel::table! {
    e_mails (id) {
        id -> Int4,
        sender_email -> Text,
        sender_lat -> Float8,
        sender_lon -> Float8,
        sender_location -> Text,
        sender_tracking -> Text,
        recipient_email -> Text,
        recipient_lat -> Float8,
        recipient_lon -> Float8,
        recipient_location -> Text,
        recipient_tracking -> Text,
        mailed_time -> Timestamp,
        posted_time -> Timestamp,
        arrival_time -> Nullable<Timestamp>,
        e_mail_body -> Text,
        opened -> Bool,
        delivery_email -> Bool,
        thread -> Nullable<Int4>,
        estimated_arrival_time -> Timestamp,
    }
}

diesel::table! {
    flights (id) {
        id -> Int4,
        flight_id -> Text,
        flight_time -> Timestamp,
        delayed_to -> Nullable<Timestamp>,
        emails_on_board -> Array<Int4>,
        origin -> Text,
        destination -> Text,
        connecting_flights -> Array<Int4>,
        has_taken_off -> Bool,
        has_landed -> Bool,
        duration -> Int4,
        flight_land_time -> Nullable<Timestamp>,
        origin_lat -> Float8,
        origin_lon -> Float8,
        destination_lat -> Float8,
        destination_lon -> Float8,
    }
}

diesel::joinable!(delivery_events -> e_mails (email_id));
diesel::joinable!(e_mails -> e_mail_threads (thread));

diesel::allow_tables_to_appear_in_same_query!(
    delivery_events,
    e_mail_threads,
    e_mails,
    flights,
);
