use axum::{
  http::StatusCode,
};

pub fn internal_error<E: std::fmt::Debug>(err: E) -> (StatusCode, String)
{
  warn!("the error inside the error: {:?}", err);
  (StatusCode::INTERNAL_SERVER_ERROR, "internal error".to_string())
}


// impl From<r2d2::Error> for (StatusCode, std::string::String) {
//   fn from(e: r2d2::Error) -> Self {
//     (StatusCode::INTERNAL_SERVER_ERROR, "eh oh, robo not happy".to_string())
//   }
// }