use crate::constants::LOCATION_WEATHER_CODES;

use futures::prelude::*;
use hyper::{body::Buf, Client, Uri};
use std::env;
use tokio::runtime::Runtime;

macro_rules! format_weather_uri {
  ($($arg:tt)*) => (format!("http://api.openweathermap.org/data/2.5/weather?id={}&APPID={}",
                      $($arg)*,
                      env::var("WEATHER_API_TOKEN").expect("WEATHER_API_TOKEN environment variable required"))
                      .parse().unwrap())
}

lazy_static! {
  static ref WEATHER_URIS: Vec<(&'static str, Uri)> = vec![
    ("ORD", format_weather_uri!(LOCATION_WEATHER_CODES["ORD"])),
    ("SFO", format_weather_uri!(LOCATION_WEATHER_CODES["SFO"])),
    ("KLI", format_weather_uri!(LOCATION_WEATHER_CODES["KLI"])),
    ("LHR", format_weather_uri!(LOCATION_WEATHER_CODES["LHR"])),
  ];
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct Weather {
  pub id: usize,
  pub main: String,
  pub description: String,
  pub icon: String,
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct WeatherResponse {
  pub weather: Vec<Weather>,
  pub id: usize,
  pub name: String,
  pub short_name: Option<String>,
}

pub fn check_airport_weather() -> Result<Vec<WeatherResponse>, anyhow::Error> {
  let core = Runtime::new()?;
  let client = Client::new();

  let futs = WEATHER_URIS.iter().cloned().map(|(short_name, uri)| {
    client
      .get(uri)
      .and_then(move |res| hyper::body::aggregate(res))
      .map(move |maybe_body| match maybe_body {
        Ok(body) => {
          let mut v: Result<WeatherResponse, _> = serde_json::from_reader(body.reader());
          if let Ok(v) = v.as_mut() {
            v.short_name = Some(short_name.into());
          }
          v.map_err(|e| anyhow!(e))
        }
        Err(e) => Err(anyhow!(e)),
      })
  });

  let results = core.block_on(future::join_all(futs));
  results
    .into_iter()
    .collect::<Result<Vec<WeatherResponse>, _>>()
}
