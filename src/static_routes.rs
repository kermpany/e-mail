use axum::{
  response::{IntoResponse}, http::StatusCode,
};

pub async fn not_found() -> impl IntoResponse {
  (StatusCode::NOT_FOUND, "welp, couldn't find what you were lookin' for...")
}

// async fn fivehundy() -> impl IntoResponse {
//   (StatusCode::INTERNAL_SERVER_ERROR, "welp, this one might be on us...")
// }
