use crate::constants;
use crate::db;
use crate::db::DbPool;
use crate::error::internal_error;

use chrono::Utc;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{SmtpTransport, Transport, Message};
use lettre::message::{header, SinglePart, MultiPart};
use std::{env, thread, time};
use tera::{Context, Tera};
// use futures::future;
use axum::http::StatusCode;

lazy_static! {
  pub static ref TERA: Tera = Tera::new("templates/**/*").unwrap();
}

pub fn send_pending_delivery_emails(pool: DbPool) -> Result<(), (StatusCode, String)> {
  let mails = db::get_new_delivered_emails(pool.clone())?;
  let smtp_host = env::var("SMTP_HOST").expect("SMTP_HOST environment variable required");
  let smtp_user = env::var("SMTP_USER").expect("SMTP_USER environment variable required");
  let smtp_pass = env::var("SMTP_PASSWORD").expect("SMTP_PASSWORD environment variable required");

  let mailer = SmtpTransport::starttls_relay(&smtp_host)
    .unwrap()
    .credentials(Credentials::new(smtp_user, smtp_pass))
    .build();

  let now = Utc::now().naive_utc();
  let mails = mails.iter().filter(|mail| match mail.arrival_time {
    Some(time) => {
      if time < now {
        true
      } else {
        false
      }
    }
    None => false,
  });
  for mail in mails {
    debug!("{:?}", mail);
    let (subject, html_template, txt_template) = (
      "Your Kermpany E-mail has arrived!",
      "email-delivered.html",
      "email-delivered.txt",
    );

    let mut context = Context::new();
    context.insert("trackingNumber", &mail.recipient_tracking);
    context.insert("senderEmail", &mail.sender_email);

    info!("sending delivery email to {}", &mail.recipient_email);

    let email = Message::builder()
      .from("Kermp Pal <kermps@e-mail.kermpany.com>".parse().unwrap())
      .to(mail.recipient_email.parse().unwrap())
      .subject(subject)
      .multipart(
        MultiPart::alternative() // This is composed of two parts.
          .singlepart(
            SinglePart::builder()
              .header(header::ContentType::TEXT_PLAIN)
              .body(
                TERA.render(txt_template, &context)
                  .expect("our templating is fooked")
              ), // Every message should have a plain text fallback.
          )
          .singlepart(
            SinglePart::builder()
              .header(header::ContentType::TEXT_HTML)
              .body(
                TERA.render(html_template, &context)
                  .expect("our templating is fooked")
              ),
          ),
      )
      .expect("failed to build email");

    mailer.send(&email)
      .map_err(internal_error)?;
    slog_scope::debug!("Email sent successfully!");

    db::mark_mail_delivered(pool.clone(), mail.id)?;
  }
  Ok(())
}


pub fn try_sending_email(
  sender_email: &str,
  email: &str,
  tracking: &str,
  is_sender: bool,
) -> Result<(), (StatusCode, String)> {
  for _ in 0..constants::EMAIL_RETRIES {
    match sending_some_email(sender_email, email, tracking, is_sender) {
      Ok(_) => return Ok(()),
      Err(e) => {
        println!("{:#?}",e);
        warn!(
          "retrying send to {} after {} seconds, error was: {:?}",
          email,
          constants::EMAIL_RETRY_SLEEP_SECS,
          e
        );
        thread::sleep(time::Duration::from_secs(constants::EMAIL_RETRY_SLEEP_SECS));
      }
    }
  }
  Err((StatusCode::INTERNAL_SERVER_ERROR, format!("tried {} times, send failed.", constants::EMAIL_RETRIES).into()))
}

pub fn sending_some_email(
  sender_email: &str,
  email: &str,
  tracking: &str,
  is_sender: bool,
) -> Result<(), (StatusCode, String)> {
  if env::var("NO_MAIL_PLEASE").unwrap_or_else(|_| "0".into()) == "1" {
    return Ok(());
  }

  let smtp_host = env::var("SMTP_HOST").expect("SMTP_HOST environment variable required");
  let smtp_user = env::var("SMTP_USER").expect("SMTP_USER environment variable required");
  let smtp_pass = env::var("SMTP_PASSWORD").expect("SMTP_PASSWORD environment variable required");

  let (subject, html_template, txt_template) = if is_sender {
    (
      "Your Kermpany E-mail tracking code!",
      "email-sender.html",
      "email-sender.txt",
    )
  } else {
    (
      "Somebody sent you a Kermpany E-mail!",
      "email-receiver.html",
      "email-receiver.txt",
    )
  };

  let mut context = Context::new();
  context.insert("trackingNumber", &tracking);
  if !is_sender {
    context.insert("sender_email", &sender_email);
  }

  info!("sending email to {}", &email[..]);
  let email = Message::builder()
    .from("Kermp Pal <kermps@e-mail.kermpany.com>".parse().unwrap())
    .to(email.parse().unwrap())
    .subject(subject)
    .multipart(
      MultiPart::alternative() // This is composed of two parts.
        .singlepart(
          SinglePart::builder()
            .header(header::ContentType::TEXT_PLAIN)
            .body(
              TERA.render(txt_template, &context)
                .expect("our templating is fooked")
            ), // Every message should have a plain text fallback.
        )
        .singlepart(
          SinglePart::builder()
            .header(header::ContentType::TEXT_HTML)
            .body(
              TERA.render(html_template, &context)
                .expect("our templating is fooked")
            ),
        ),
    )
    .expect("failed to build email");
  
  let mailer = SmtpTransport::relay(&smtp_host)
    .unwrap()
    .credentials(Credentials::new(smtp_user, smtp_pass))
    .build();

  mailer.send(&email)
    .map_err(internal_error)?;
  slog_scope::debug!("Email sent successfully!");
  Ok(())
}

