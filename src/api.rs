use crate::datastructs::{EmailRequest, ThreadInfo, TrackingInfo, TrackingNumbers};
use crate::db;
use crate::db::DbPool;
use crate::email;
use crate::weather::WeatherResponse;

use axum::{extract::State, Json, http::StatusCode, extract::Path};
use futures::{future, executor::ThreadPool};


lazy_static! {
  /// This pool's work is I/O-bound. Rocket maintains its own thread pool for
  /// its jazz. Sending emails is pretty much entirely I/O, so these threads
  /// should be mostly parked. Setting to 4 so that our spam is rate-limited.
  static ref EMAIL_POOL: ThreadPool = ThreadPool::builder().pool_size(4).create().unwrap();
}

#[debug_handler]
pub async fn get_tracking_info(
  State(pool): State<DbPool>,
  Path(tracking): Path<String>,
) -> Result<Json<TrackingInfo>, (StatusCode, String)> {
  let info = db::grab_tracking_data(pool.clone(), tracking)?;
  Ok(Json(info))
}

pub async fn get_e_mail(
  State(pool): State<db::DbPool>,
  Path(tracking): Path<String>,
) -> Result<Json<ThreadInfo>, (StatusCode, String)> {
  let letter_info = db::get_e_mail(pool.clone(), tracking.clone()).await?;

  Ok(Json(letter_info))
}

#[debug_handler]
pub async fn new_e_mail(
  State(pool): State<db::DbPool>,
  Json(email): Json<EmailRequest>,
) -> Result<Json<TrackingNumbers>, (StatusCode, String)> {
  let e_mail = db::save_edashmail(pool.clone(), &email)?;

  let _ = db::create_delivery_path(pool.clone(), e_mail.id);

  // send the email for the e_mail
  let sender_email = e_mail.sender_email.clone();
  let sender_tracking = e_mail.sender_tracking.clone();
  let recipient_email = e_mail.recipient_email.clone();
  let recipient_tracking = e_mail.recipient_tracking.clone();
  EMAIL_POOL.spawn_ok(future::lazy(move |_| {
    let send_res =
      email::try_sending_email(&sender_email, &sender_email, &sender_tracking, true);
    let recv_res =
      email::try_sending_email(&sender_email, &recipient_email, &recipient_tracking, false);
    match (&send_res, &recv_res) {
      (&Ok(_), &Ok(_)) => {
        slog_scope::info!("emails to {} and {} sent!", &sender_email, &recipient_email);
      }
      _ => {
        if send_res.is_err() {
          slog_scope::warn!("email to {} failed to send", &sender_email);
        }
        if recv_res.is_err() {
          slog_scope::warn!("email to {} failed to send", &recipient_email);
        }
    }
    }
  }));
  Ok(Json(TrackingNumbers::new(e_mail.sender_tracking).unwrap()))
}

#[debug_handler]
pub async fn get_weather_ticker() -> Json<Vec<WeatherResponse>> {
  let ticker_data = db::get_ticker_data();
  Json(ticker_data)
}
