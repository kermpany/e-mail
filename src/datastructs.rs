use crate::constants;
use crate::models::{DeliveryEvent, Flight};
use chrono::NaiveDateTime;

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct EmailRequest {
  pub sender: Person,
  pub recipient: Person,
  pub body: String,
  pub prev_tracking: Option<String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Person {
  pub email: String,
  pub location: Location,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Location {
  pub name: String,
  pub lat: f64,
  pub lon: f64,
}

#[derive(Debug, serde::Serialize)]
pub struct TrackingInfo {
  pub state: String,
  pub origin: String,
  pub origin_lat: f64,
  pub origin_lon: f64,
  pub destination: String,
  pub destination_lat: f64,
  pub destination_lon: f64,
  pub progress: f64,
  pub time_mailed: NaiveDateTime,
  pub time_posted: NaiveDateTime,
  pub eta: NaiveDateTime,
  pub flight_path: Vec<Flight>,
  pub events: Vec<DeliveryEvent>,
  pub sender_email: String,
  pub recipient_email: String,
}

#[derive(Debug, serde::Serialize)]
pub struct ThreadInfo {
  pub tracking_info: TrackingInfo,
  pub letter: String,
  pub letters: Vec<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct TrackingNumbers {
  pub number: String,
}

impl TrackingNumbers {
  pub fn new(number: String) -> Result<Self, anyhow::Error> {
    if !Self::is_valid(&number) {
      Err(anyhow!("not valid tracking number"))
    } else {
      Ok(TrackingNumbers { number })
    }
  }

  fn is_valid(s: &str) -> bool {
    constants::TRACKING_NUMBER_REGEX.is_match(s)
  }

}

// impl<'r> FromParam<'r> for TrackingNumbers {
//   type Error = Status;

//   fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
//     TrackingNumbers::new(param.to_string()).map_err(|_| Status::BadRequest)
//   }
// }