use crate::datastructs::Location;

use chrono::NaiveTime;
use std::collections::HashMap;

#[derive(Debug, serde::Deserialize)]
pub struct Airport {
  pub location: Location,
  pub code: String,
  pub flights: HashMap<String, Vec<Route>>,
}

#[derive(Debug, serde::Deserialize)]
pub struct Route {
  pub stop: String,
  pub duration: i32,
  pub flight_id: String,
  pub times: Vec<NaiveTime>,
}

pub fn get_kermp_air() -> HashMap<String, Airport> {
  let mut kermp_air = HashMap::new();
  let mut ord_flights = HashMap::new();
  let mut sfo_flights = HashMap::new();
  let mut lhr_flights = HashMap::new();
  let mut kli_flights = HashMap::new();

  ord_flights.insert(
    "LHR".into(),
    vec![Route {
      stop: "LHR".into(),
      duration: 27900,
      flight_id: "chi700".into(),
      times: vec![
        NaiveTime::from_hms_opt(4, 0, 0).unwrap(),
        NaiveTime::from_hms_opt(12, 5, 0).unwrap(),
        NaiveTime::from_hms_opt(17, 20, 0).unwrap(),
      ],
    }],
  );
  ord_flights.insert(
    "SFO".into(),
    vec![Route {
      stop: "SFO".into(),
      duration: 16488,
      flight_id: "chi890".into(),
      times: vec![
        NaiveTime::from_hms_opt(2, 15, 0).unwrap(),
        NaiveTime::from_hms_opt(8, 35, 0).unwrap(),
        NaiveTime::from_hms_opt(15, 15, 0).unwrap(),
      ],
    }],
  );
  ord_flights.insert(
    "KLI".into(),
    vec![
      Route {
        stop: "LHR".into(),
        duration: 27900,
        flight_id: "chi700".into(),
        times: vec![
          NaiveTime::from_hms_opt(4, 0, 0).unwrap(),
          NaiveTime::from_hms_opt(12, 5, 0).unwrap(),
          NaiveTime::from_hms_opt(17, 20, 0).unwrap(),
        ],
      },
      Route {
        stop: "KLI".into(),
        duration: 47988,
        flight_id: "lon415".into(),
        times: vec![
          NaiveTime::from_hms_opt(11, 25, 0).unwrap(),
          NaiveTime::from_hms_opt(22, 35, 0).unwrap(),
        ],
      },
    ],
  );
  ord_flights.insert("ORD".into(), vec![]);

  kermp_air.insert(
    "ORD".into(),
    Airport {
      location: Location {
        name: "Chicago O'hare Airport".into(),
        lat: 41.975381,
        lon: -87.901671,
      },
      code: "ORD".into(),
      flights: ord_flights,
    },
  );

  sfo_flights.insert(
    "LHR".into(),
    vec![Route {
      stop: "LHR".into(),
      duration: 36900,
      flight_id: "sfo700".into(),
      times: vec![
        NaiveTime::from_hms_opt(5, 55, 0).unwrap(),
        NaiveTime::from_hms_opt(13, 40, 0).unwrap(),
      ],
    }],
  );
  sfo_flights.insert(
    "ORD".into(),
    vec![Route {
      stop: "ORD".into(),
      duration: 15588,
      flight_id: "sfo650".into(),
      times: vec![
        NaiveTime::from_hms_opt(7, 30, 0).unwrap(),
        NaiveTime::from_hms_opt(17, 59, 0).unwrap(),
      ],
    }],
  );
  sfo_flights.insert(
    "KLI".into(),
    vec![
      Route {
        stop: "LHR".into(),
        duration: 36900,
        flight_id: "sfo700".into(),
        times: vec![
          NaiveTime::from_hms_opt(5, 55, 0).unwrap(),
          NaiveTime::from_hms_opt(13, 40, 0).unwrap(),
        ],
      },
      Route {
        stop: "KLI".into(),
        duration: 47988,
        flight_id: "lon415".into(),
        times: vec![
          NaiveTime::from_hms_opt(11, 25, 0).unwrap(),
          NaiveTime::from_hms_opt(22, 35, 0).unwrap(),
        ],
      },
    ],
  );
  sfo_flights.insert("SFO".into(), vec![]);

  kermp_air.insert(
    "SFO".into(),
    Airport {
      location: Location {
        name: "San Francisco International Airport".into(),
        lat: 37.618624,
        lon: -122.379649,
      },
      code: "SFO".into(),
      flights: sfo_flights,
    },
  );

  lhr_flights.insert(
    "ORD".into(),
    vec![Route {
      stop: "ORD".into(),
      duration: 30888,
      flight_id: "lon650".into(),
      times: vec![
        NaiveTime::from_hms_opt(8, 45, 0).unwrap(),
        NaiveTime::from_hms_opt(18, 15, 0).unwrap(),
      ],
    }],
  );
  lhr_flights.insert(
    "SFO".into(),
    vec![Route {
      stop: "SFO".into(),
      duration: 39600,
      flight_id: "lon890".into(),
      times: vec![
        NaiveTime::from_hms_opt(11, 25, 0).unwrap(),
        NaiveTime::from_hms_opt(15, 10, 0).unwrap(),
      ],
    }],
  );
  lhr_flights.insert(
    "KLI".into(),
    vec![Route {
      stop: "KLI".into(),
      duration: 47988,
      flight_id: "lon415".into(),
      times: vec![
        NaiveTime::from_hms_opt(11, 25, 0).unwrap(),
        NaiveTime::from_hms_opt(22, 35, 0).unwrap(),
      ],
    }],
  );
  lhr_flights.insert("LHR".into(), vec![]);

  kermp_air.insert(
    "LHR".into(),
    Airport {
      location: Location {
        name: "London Heathrow Airport".into(),
        lat: 51.470219,
        lon: -0.454558,
      },
      code: "LHR".into(),
      flights: lhr_flights,
    },
  );

  kli_flights.insert(
    "LHR".into(),
    vec![Route {
      stop: "LHR".into(),
      duration: 49176,
      flight_id: "kl700".into(),
      times: vec![
        NaiveTime::from_hms_opt(7, 15, 0).unwrap(),
        NaiveTime::from_hms_opt(18, 50, 0).unwrap(),
      ],
    }],
  );
  kli_flights.insert(
    "SFO".into(),
    vec![
      Route {
        stop: "LHR".into(),
        duration: 49176,
        flight_id: "kl700".into(),
        times: vec![
          NaiveTime::from_hms_opt(7, 15, 0).unwrap(),
          NaiveTime::from_hms_opt(18, 50, 0).unwrap(),
        ],
      },
      Route {
        stop: "SFO".into(),
        duration: 16488,
        flight_id: "lon890".into(),
        times: vec![
          NaiveTime::from_hms_opt(11, 25, 0).unwrap(),
          NaiveTime::from_hms_opt(15, 10, 0).unwrap(),
        ],
      },
    ],
  );
  kli_flights.insert(
    "ORD".into(),
    vec![
      Route {
        stop: "LHR".into(),
        duration: 49176,
        flight_id: "kl700".into(),
        times: vec![
          NaiveTime::from_hms_opt(7, 15, 0).unwrap(),
          NaiveTime::from_hms_opt(18, 50, 0).unwrap(),
        ],
      },
      Route {
        stop: "ORD".into(),
        duration: 30888,
        flight_id: "lon650".into(),
        times: vec![
          NaiveTime::from_hms_opt(8, 45, 0).unwrap(),
          NaiveTime::from_hms_opt(18, 15, 0).unwrap(),
        ],
      },
    ],
  );
  kli_flights.insert("KLI".into(), vec![]);

  kermp_air.insert(
    "KLI".into(),
    Airport {
      location: Location {
        name: "Kuala Lumpur International Airport".into(),
        lat: 2.739468,
        lon: 101.705757,
      },
      code: "KLI".into(),
      flights: kli_flights,
    },
  );

  kermp_air
}
