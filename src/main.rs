use e_mail::api;
use e_mail::constants::BAD_WEATHER_CODES;
use e_mail::db::DbPool;
use e_mail::email;
use e_mail::static_routes;

use axum::{
  handler::HandlerWithoutStateExt,
  routing::{get, post},
  Router,
};
use diesel::{
  r2d2::{ConnectionManager, Pool},
  PgConnection,
};
use dotenv::dotenv;
use slog::Drain;
use std::{thread, time};
use tower_http::services::{ServeDir, ServeFile};

#[tokio::main]
async fn main() {
  let decorator = slog_term::TermDecorator::new().build();
  let drain = slog_term::CompactFormat::new(decorator).build().fuse();
  let drain = slog_async::Async::new(drain).build().fuse();
  let log = slog::Logger::root(drain, slog::slog_o!());
  let _guard = slog_scope::set_global_logger(log);

  dotenv().ok();
  let db_url = std::env::var("DATABASE_URL").unwrap();
  let manager = ConnectionManager::<PgConnection>::new(db_url);
  let db_pool = Pool::builder()
    .max_size(8)
    .build(manager)
    .expect("Failed to create pool.");

  start_delivery_email_thread(db_pool.clone());
  start_weather_loops(db_pool.clone());

  let static_dir =
    ServeDir::new("static").not_found_service(static_routes::not_found.into_service());

  let static_routes = Router::new()
    .route_service("/tracking", ServeFile::new("static/tracking.html"))
    .route_service("/tracking/",ServeFile::new("static/tracking.html"))
    .route_service(
      "/tracking/:tracking",
      ServeFile::new("static/tracking.html"),
    )
    .route_service("/e-mail", ServeFile::new("static/e-mail.html"))
    .route_service("/e-mail/",ServeFile::new("static/e-mail.html"))
    .route_service("/e-mail/:tracking", ServeFile::new("static/e-mail.html"))
    .route_service("/reply", ServeFile::new("static/index.html"));

  let api_routes = Router::new()
    .route("/e-mail/:tracking", get(api::get_e_mail))
    .route("/e-mail", post(api::new_e_mail))
    .route("/tracking_info/:tracking", get(api::get_tracking_info))
    .route("/weather_ticker", get(api::get_weather_ticker));

  let app = Router::new()
    .merge(static_routes)
    .nest("/api", api_routes)
    .layer(tower_http::catch_panic::CatchPanicLayer::new()) // https://docs.rs/tower-http/0.3.5/tower_http/catch_panic/index.html
    .fallback_service(static_dir)
    .with_state(db_pool);

  axum::Server::bind(&"0.0.0.0:8000".parse().unwrap())
    .serve(app.into_make_service())
    .await
    .unwrap();
}

fn start_delivery_email_thread(pool: DbPool) {
  thread::spawn(move || {
    slog_scope::info!("delivery email thread spun up");
    loop {
      slog_scope::debug!("checking for new delivery emails to send...");
      match email::send_pending_delivery_emails(pool.clone()) {
        Err(e) => slog_scope::warn!("send_pending_delivery_emails Err: {:?}", e),
        _ => (),
      }
      thread::sleep(time::Duration::from_secs(5 * 60));
    }
  });
}

fn start_weather_loops(pool: DbPool) {
  thread::spawn(move || {
    slog_scope::info!("weather loop thread spinnnnnning");
    loop {
      slog_scope::debug!("checking for weather womps");
      match e_mail::weather::check_airport_weather() {
        Ok(wedde_arr) => {
          e_mail::db::update_ticker(wedde_arr.clone());
          let mut smooth_sailers = vec![];
          for wedde in wedde_arr.iter() {
            if BAD_WEATHER_CODES
              .iter()
              .find(|&&x| x == wedde.weather[0].id)
              .is_some()
            {
              slog_scope::debug!("icky weather in {}", wedde.name);
              if let Err(e) = e_mail::db::update_affected_flight(pool.clone(), wedde) {
                slog_scope::warn!("bumped into an uhoh updating routes: {:?}", e);
              }
            } else {
              slog_scope::debug!("smooth sailing in {}", wedde.name);
              smooth_sailers.push(wedde.short_name.clone().unwrap());
            }
          }
          if let Err(e) = e_mail::db::new_flights_taking_off(pool.clone(), smooth_sailers) {
            slog_scope::warn!("error in db::new_flights_taking_off(): {:?}", e);
          }
          if let Err(e) = e_mail::db::new_drones_taking_off(pool.clone()) {
            slog_scope::warn!("error in db::new_flights_taking_off(): {:?}", e);
          }
          if let Err(e) = e_mail::db::new_flights_landing(pool.clone()) {
            slog_scope::warn!("error in db::new_flights_landing(): {:?}", e);
          }
          if let Err(e) = e_mail::db::new_drones_landing(pool.clone()) {
            slog_scope::warn!("error in db::new_flights_landing(): {:?}", e);
          }
        }
        Err(e) => slog_scope::warn!("bad things happening: {:?}", e),
      }
      thread::sleep(time::Duration::from_secs(15 * 60));
      // thread::sleep(time::Duration::from_secs(5 * 60));
    }
  });
}
