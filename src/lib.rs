#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate anyhow;

#[macro_use]
extern crate axum_macros;

extern crate slog;
#[macro_use]
extern crate slog_scope;
extern crate slog_term;
extern crate slog_async;


pub mod api;
pub mod constants;
pub mod datastructs;
pub mod db;
pub mod db_filler;
pub mod email;
pub mod error;
pub mod flights;
pub mod helpers;
pub mod models;
pub mod schema;
pub mod static_routes;
pub mod weather;