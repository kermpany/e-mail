-- This file should undo anything in `up.sql`

ALTER TABLE flights DROP COLUMN origin_lat;
ALTER TABLE flights DROP COLUMN origin_lon;
ALTER TABLE flights DROP COLUMN destination_lat;
ALTER TABLE flights DROP COLUMN destination_lon;
ALTER TABLE flights RENAME COLUMN origin TO start_airport;
ALTER TABLE flights RENAME COLUMN destination TO end_airport;
