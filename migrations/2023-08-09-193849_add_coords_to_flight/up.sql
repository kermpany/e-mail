
ALTER TABLE flights ADD COLUMN origin_lat DOUBLE PRECISION NOT NULL DEFAULT 0.0;
ALTER TABLE flights ADD COLUMN origin_lon DOUBLE PRECISION NOT NULL DEFAULT 0.0;
ALTER TABLE flights ADD COLUMN destination_lat DOUBLE PRECISION NOT NULL DEFAULT 0.0;
ALTER TABLE flights ADD COLUMN destination_lon DOUBLE PRECISION NOT NULL DEFAULT 0.0;
ALTER TABLE flights RENAME COLUMN start_airport TO origin;
ALTER TABLE flights RENAME COLUMN end_airport TO destination;

UPDATE flights SET origin_lat = 41.975381, origin_lon = -87.901671 where origin = 'ORD';
UPDATE flights SET origin_lat = 37.618624, origin_lon = -122.379649 where origin = 'SFO';
UPDATE flights SET origin_lat = 51.470219, origin_lon = -0.454558 where origin = 'LHR';
UPDATE flights SET origin_lat = 2.739468, origin_lon = 101.705757 where origin = 'KLI';

UPDATE flights SET destination_lat = 41.975381, destination_lon = -87.901671 where destination = 'ORD';
UPDATE flights SET destination_lat = 37.618624, destination_lon = -122.379649 where destination = 'SFO';
UPDATE flights SET destination_lat = 51.470219, destination_lon = -0.454558 where destination = 'LHR';
UPDATE flights SET destination_lat = 2.739468, destination_lon = 101.705757 where destination = 'KLI';

UPDATE flights SET destination_lat = e_mails.recipient_lat, destination_lon = e_mails.recipient_lon from e_mails where destination not in ('KLI', 'ORD', 'SFO', 'LHR')
AND ARRAY[e_mails.id] = flights.emails_on_board;


UPDATE flights SET origin_lat = e_mails.sender_lat, origin_lon = e_mails.sender_lon from e_mails where origin not in ('KLI', 'ORD', 'SFO', 'LHR')
AND ARRAY[e_mails.id] = flights.emails_on_board;
