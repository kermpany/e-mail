CREATE TABLE weather_ticker (
    id SERIAL PRIMARY KEY,
    ticker_blob JSONB NOT NULL,
    created_at TIMESTAMP NOT NULL
);