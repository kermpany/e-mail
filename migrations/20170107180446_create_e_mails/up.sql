CREATE TABLE e_mails (
  id SERIAL PRIMARY KEY,
  sender_email TEXT NOT NULL,
  sender_lat DOUBLE PRECISION NOT NULL,
  sender_lon DOUBLE PRECISION NOT NULL,
  sender_location TEXT NOT NULL,
  sender_tracking TEXT NOT NULL,
  recipient_email TEXT NOT NULL,
  recipient_lat DOUBLE PRECISION NOT NULL,
  recipient_lon DOUBLE PRECISION NOT NULL,
  recipient_location TEXT NOT NULL,
  recipient_tracking TEXT NOT NULL,
  mailed_time TIMESTAMP NOT NULL,
  posted_time TIMESTAMP NOT NULL,
  arrival_time TIMESTAMP NOT NULL,
  e_mail_body TEXT NOT NULL,
  opened BOOLEAN NOT NULL DEFAULT FALSE,
  delivery_email BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE UNIQUE INDEX sender_tracking_idx on e_mails (sender_tracking);
CREATE UNIQUE INDEX recipient_tracking_idx on e_mails (recipient_tracking);
