CREATE TABLE delivery_events (
  id SERIAL PRIMARY KEY,
  email_id INT REFERENCES e_mails (id) NOT NULL,
  event_text TEXT NOT NULL,
  event_time TIMESTAMP NOT NULL
);
