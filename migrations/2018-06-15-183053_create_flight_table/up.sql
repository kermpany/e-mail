-- Your SQL goes here
CREATE TABLE flights (
    id SERIAL PRIMARY KEY,
    flight_id TEXT NOT NULL,
    flight_time TIMESTAMP NOT NULL,
    delayed_to TIMESTAMP,
    emails_on_board INT[] NOT NULL, -- List of email ids
    start_airport TEXT NOT NULL,
    end_airport TEXT NOT NULL,
    connecting_flights INT[] -- flight ids if email on next flight
)