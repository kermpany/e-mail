ALTER TABLE delivery_events DROP is_delayed;
ALTER TABLE delivery_events ADD COLUMN event_type TEXT NOT NULL DEFAULT 'none';