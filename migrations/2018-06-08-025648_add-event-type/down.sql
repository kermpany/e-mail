ALTER TABLE delivery_events ADD COLUMN is_delayed BOOLEAN DEFAULT FALSE;
ALTER TABLE delivery_events DROP event_type;