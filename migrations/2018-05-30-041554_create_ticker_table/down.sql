
ALTER TABLE delivery_events DROP is_delayed;
ALTER TABLE delivery_events DROP delayed_to;
ALTER TABLE delivery_events DROP closest_airport;
DROP TABLE weather_ticker;