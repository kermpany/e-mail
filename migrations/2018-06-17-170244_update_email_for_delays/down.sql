-- This file should undo anything in `up.sql`
UPDATE e_mails SET arrival_time = estimated_arrival_time WHERE arrival_time is NULL;
ALTER TABLE e_mails ALTER COLUMN arrival_time SET NOT NULL;
ALTER TABLE e_mails DROP COLUMN estimated_arrival_time;
ALTER TABLE flights DROP COLUMN has_taken_off;
ALTER TABLE flights DROP COLUMN has_landed;
ALTER TABLE flights DROP COLUMN duration;
ALTER TABLE flights DROP COLUMN flight_land_time;
ALTER TABLE flights DROP CONSTRAINT unique_flight;
ALTER TABLE flights ALTER connecting_flights DROP NOT NULL;
ALTER TABLE delivery_events ADD COLUMN delayed_to TIMESTAMP;