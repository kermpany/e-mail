-- Your SQL goes here
ALTER TABLE e_mails ALTER arrival_time DROP NOT NULL;
ALTER TABLE e_mails ADD COLUMN estimated_arrival_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
UPDATE e_mails SET estimated_arrival_time = arrival_time;
ALTER TABLE flights ADD COLUMN has_taken_off BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE flights ADD COLUMN has_landed BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE flights ADD COLUMN duration INT NOT NULL DEFAULT 10000;
ALTER TABLE flights ADD COLUMN flight_land_time TIMESTAMP;
ALTER TABLE flights ADD CONSTRAINT unique_flight UNIQUE(start_airport, end_airport, flight_time);
ALTER TABLE flights ALTER connecting_flights SET NOT NULL;
ALTER TABLE flights ALTER connecting_flights SET DEFAULT ARRAY[]::INT[];
ALTER TABLE delivery_events DROP COLUMN delayed_to;