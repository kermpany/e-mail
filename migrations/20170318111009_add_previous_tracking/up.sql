CREATE TABLE e_mail_threads (
  id SERIAL PRIMARY KEY,
  tracking_nums TEXT[] NOT NULL DEFAULT '{}'
);

ALTER TABLE e_mails ADD COLUMN thread int references e_mail_threads(id);